namespace SSMono;
        // class declarations
         class UriBuilder;
         class UriParser;
         class GenericUriParser;
         class GenericUriParserOptions;
     class UriBuilder 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Fragment[];
        STRING Host[];
        STRING Password[];
        STRING Path[];
        SIGNED_LONG_INTEGER Port;
        STRING Query[];
        STRING Scheme[];
        STRING UserName[];
    };

     class UriParser 
    {
        // class delegates

        // class events

        // class functions
        static FUNCTION Register ( UriParser uriParser , STRING schemeName , SIGNED_LONG_INTEGER defaultPort );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class GenericUriParserOptions // enum
    {
        static SIGNED_LONG_INTEGER Default;
        static SIGNED_LONG_INTEGER GenericAuthority;
        static SIGNED_LONG_INTEGER AllowEmptyAuthority;
        static SIGNED_LONG_INTEGER NoUserInfo;
        static SIGNED_LONG_INTEGER NoPort;
        static SIGNED_LONG_INTEGER NoQuery;
        static SIGNED_LONG_INTEGER NoFragment;
        static SIGNED_LONG_INTEGER DontConvertPathBackslashes;
        static SIGNED_LONG_INTEGER DontCompressPath;
        static SIGNED_LONG_INTEGER DontUnescapePathDotsAndSlashes;
        static SIGNED_LONG_INTEGER Idn;
        static SIGNED_LONG_INTEGER IriParsing;
    };

namespace SSMono.Net.Sockets;
        // class declarations
         class SocketType;
         class BufferedNetworkStream;
         class NetworkStream;
         class SocketAddress;
         class SocketError;
         class SocketResultExtensions;
         class TransmitFileOptions;
         class ProtocolType;
         class SocketFlags;
         class SocketShutdown;
         class EndPoint;
         class SocketException;
    static class SocketType // enum
    {
        static SIGNED_LONG_INTEGER Stream;
        static SIGNED_LONG_INTEGER Dgram;
        static SIGNED_LONG_INTEGER Raw;
        static SIGNED_LONG_INTEGER Rdm;
        static SIGNED_LONG_INTEGER Seqpacket;
        static SIGNED_LONG_INTEGER Unknown;
    };

    static class SocketError // enum
    {
        static SIGNED_LONG_INTEGER Success;
        static SIGNED_LONG_INTEGER OperationAborted;
        static SIGNED_LONG_INTEGER IOPending;
        static SIGNED_LONG_INTEGER Interrupted;
        static SIGNED_LONG_INTEGER AccessDenied;
        static SIGNED_LONG_INTEGER Fault;
        static SIGNED_LONG_INTEGER InvalidArgument;
        static SIGNED_LONG_INTEGER TooManyOpenSockets;
        static SIGNED_LONG_INTEGER WouldBlock;
        static SIGNED_LONG_INTEGER InProgress;
        static SIGNED_LONG_INTEGER AlreadyInProgress;
        static SIGNED_LONG_INTEGER NotSocket;
        static SIGNED_LONG_INTEGER DestinationAddressRequired;
        static SIGNED_LONG_INTEGER MessageSize;
        static SIGNED_LONG_INTEGER ProtocolType;
        static SIGNED_LONG_INTEGER ProtocolOption;
        static SIGNED_LONG_INTEGER ProtocolNotSupported;
        static SIGNED_LONG_INTEGER SocketNotSupported;
        static SIGNED_LONG_INTEGER OperationNotSupported;
        static SIGNED_LONG_INTEGER ProtocolFamilyNotSupported;
        static SIGNED_LONG_INTEGER AddressFamilyNotSupported;
        static SIGNED_LONG_INTEGER AddressAlreadyInUse;
        static SIGNED_LONG_INTEGER AddressNotAvailable;
        static SIGNED_LONG_INTEGER NetworkDown;
        static SIGNED_LONG_INTEGER NetworkUnreachable;
        static SIGNED_LONG_INTEGER NetworkReset;
        static SIGNED_LONG_INTEGER ConnectionAborted;
        static SIGNED_LONG_INTEGER ConnectionReset;
        static SIGNED_LONG_INTEGER NoBufferSpaceAvailable;
        static SIGNED_LONG_INTEGER IsConnected;
        static SIGNED_LONG_INTEGER NotConnected;
        static SIGNED_LONG_INTEGER Shutdown;
        static SIGNED_LONG_INTEGER TimedOut;
        static SIGNED_LONG_INTEGER ConnectionRefused;
        static SIGNED_LONG_INTEGER HostDown;
        static SIGNED_LONG_INTEGER HostUnreachable;
        static SIGNED_LONG_INTEGER ProcessLimit;
        static SIGNED_LONG_INTEGER SystemNotReady;
        static SIGNED_LONG_INTEGER VersionNotSupported;
        static SIGNED_LONG_INTEGER NotInitialized;
        static SIGNED_LONG_INTEGER Disconnecting;
        static SIGNED_LONG_INTEGER TypeNotFound;
        static SIGNED_LONG_INTEGER HostNotFound;
        static SIGNED_LONG_INTEGER TryAgain;
        static SIGNED_LONG_INTEGER NoRecovery;
        static SIGNED_LONG_INTEGER NoData;
        static SIGNED_LONG_INTEGER SocketError;
    };

    static class SocketResultExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class TransmitFileOptions // enum
    {
        static SIGNED_LONG_INTEGER UseDefaultWorkerThread;
        static SIGNED_LONG_INTEGER Disconnect;
        static SIGNED_LONG_INTEGER ReuseSocket;
        static SIGNED_LONG_INTEGER WriteBehind;
        static SIGNED_LONG_INTEGER UseSystemThread;
        static SIGNED_LONG_INTEGER UseKernelApc;
    };

    static class ProtocolType // enum
    {
        static SIGNED_LONG_INTEGER IP;
        static SIGNED_LONG_INTEGER Icmp;
        static SIGNED_LONG_INTEGER Igmp;
        static SIGNED_LONG_INTEGER Ggp;
        static SIGNED_LONG_INTEGER Tcp;
        static SIGNED_LONG_INTEGER Pup;
        static SIGNED_LONG_INTEGER Udp;
        static SIGNED_LONG_INTEGER Idp;
        static SIGNED_LONG_INTEGER IPv6;
        static SIGNED_LONG_INTEGER ND;
        static SIGNED_LONG_INTEGER Raw;
        static SIGNED_LONG_INTEGER Ipx;
        static SIGNED_LONG_INTEGER Spx;
        static SIGNED_LONG_INTEGER SpxII;
        static SIGNED_LONG_INTEGER Unknown;
    };

    static class SocketFlags // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER OutOfBand;
        static SIGNED_LONG_INTEGER Peek;
        static SIGNED_LONG_INTEGER DontRoute;
        static SIGNED_LONG_INTEGER MaxIOVectorLength;
        static SIGNED_LONG_INTEGER Truncated;
        static SIGNED_LONG_INTEGER ControlDataTruncated;
        static SIGNED_LONG_INTEGER Broadcast;
        static SIGNED_LONG_INTEGER Multicast;
        static SIGNED_LONG_INTEGER Partial;
    };

    static class SocketShutdown // enum
    {
        static SIGNED_LONG_INTEGER Receive;
        static SIGNED_LONG_INTEGER Send;
        static SIGNED_LONG_INTEGER Both;
    };

     class EndPoint 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class SocketException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER ErrorCode;
        SocketError SocketErrorCode;
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

namespace SSMono.Security.Principal;
        // class declarations
         class GenericPrincipal;
         class GenericIdentity;

namespace Crestron.SimplSharp.CrestronSockets;
        // class declarations
         class CrestronSocket;
         class CrestronServerSocket;
         class CrestronListenerSocket;
         class SocketSendAsyncResult;
         class SocketReceiveAsyncResult;
         class SocketAcceptAsyncResult;
         class CrestronConnectableSocket;
         class CrestronNetExtensions;
         class CrestronClientSocket;
         class CrestronUdpSocket;
     class CrestronSocket 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION Shutdown ( SocketShutdown how );
        FUNCTION SendFile ( STRING fileName );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER SendTimeout;
        SIGNED_LONG_INTEGER ReceiveTimeout;
        SocketType SocketType;
        ProtocolType ProtocolType;
    };

     class CrestronListenerSocket 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION Start ( SIGNED_LONG_INTEGER backlog );
        FUNCTION Listen ();
        FUNCTION Stop ();
        FUNCTION Dispose ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class SocketSendAsyncResult 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class SocketReceiveAsyncResult 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class SocketAcceptAsyncResult 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class CrestronConnectableSocket 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Connect ( STRING hostname , SIGNED_LONG_INTEGER port );
        FUNCTION Close ();
        FUNCTION Shutdown ( SocketShutdown how );
        FUNCTION SendFile ( STRING fileName );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER SendTimeout;
        SIGNED_LONG_INTEGER ReceiveTimeout;
        SocketType SocketType;
        ProtocolType ProtocolType;
    };

    static class CrestronNetExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class CrestronClientSocket 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION Connect ( STRING hostname , SIGNED_LONG_INTEGER port );
        FUNCTION Shutdown ( SocketShutdown how );
        FUNCTION SendFile ( STRING fileName );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        ProtocolType ProtocolType;
        SocketType SocketType;
        SIGNED_LONG_INTEGER SendTimeout;
        SIGNED_LONG_INTEGER ReceiveTimeout;
    };

     class CrestronUdpSocket 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION Connect ( STRING hostname , SIGNED_LONG_INTEGER port );
        FUNCTION Shutdown ( SocketShutdown how );
        FUNCTION SendFile ( STRING fileName );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        ProtocolType ProtocolType;
        SocketType SocketType;
        SIGNED_LONG_INTEGER SendTimeout;
        SIGNED_LONG_INTEGER ReceiveTimeout;
    };

namespace SSMono.Net;
        // class declarations
         class CookieContainer;
         class CookieCollection;
         class IPUtilities;
         class IPAddressTryParser;
         class IPHostEntry;
         class CookieException;
         class NetworkCredential;
         class Cookie;
     class CookieContainer 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Add ( Cookie cookie );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static SIGNED_LONG_INTEGER DefaultCookieLengthLimit;
        static SIGNED_LONG_INTEGER DefaultCookieLimit;
        static SIGNED_LONG_INTEGER DefaultPerDomainCookieLimit;

        // class properties
        SIGNED_LONG_INTEGER Count;
        SIGNED_LONG_INTEGER Capacity;
        SIGNED_LONG_INTEGER MaxCookieSize;
        SIGNED_LONG_INTEGER PerDomainCapacity;
    };

     class CookieCollection 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION CopyTo ( Cookie array[] , SIGNED_LONG_INTEGER index );
        FUNCTION Add ( Cookie cookie );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Count;
    };

    static class IPUtilities 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class IPAddressTryParser 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class IPHostEntry 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Aliases[][];
        STRING HostName[];
    };

     class CookieException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

     class NetworkCredential 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Domain[];
        STRING UserName[];
        STRING Password[];
    };

     class Cookie 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Comment[];
        STRING Domain[];
        STRING Name[];
        STRING Path[];
        STRING Port[];
        STRING Value[];
        SIGNED_LONG_INTEGER Version;
    };

