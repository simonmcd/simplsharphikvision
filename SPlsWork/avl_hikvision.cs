using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;
using HikvisionLibrary;
using SimplSharpHTTPLibrary;
using System.Text;
using System;
using Crestron.SimplSharp.CrestronIO;
using Crestron.SimplSharp;
using OpenNETCF;
using Crestron.SimplSharp.Reflection.Emit;
using Crestron.SimplSharp.Reflection;
using SSMono.Security.Cryptography;
using Mono.Security.Cryptography;
using SSMono.Xml;
using SSMono.Security.AccessControl;
using Mono.Math;
using SSMono.Security;
using Mono.Math.Prime;
using Mono.Security;
using Mono.Math.Prime.Generator;
using SSMono;
using SSMono.Net.Sockets;
using SSMono.Security.Principal;
using Crestron.SimplSharp.CrestronSockets;
using SSMono.Net;
using SSMono.Net.Cache;
using SSMono.Net.Security;
using Mono.Security.Protocol.Tls;
using SSMono.Security.Authentication;
using SSMono.Security.Permissions;
using SSMono.ComponentModel;
using SSMono.Web.UI;
using SSMono.Threading;
using System.Collections;
using SSMono.Security.Cryptography.X509Certificates;
using Mono.Security.X509;
using Mono.Security.X509.Extensions;
using Mono.Security.Authenticode;

namespace UserModule_AVL_HIKVISION
{
    public class UserModuleClass_AVL_HIKVISION : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        StringParameter IPADDRESS;
        UShortParameter PORT;
        StringParameter USERNAME;
        StringParameter PASSWORD;
        Crestron.Logos.SplusObjects.DigitalInput POLL;
        HikvisionLibrary.HikvisionClient MYHIKVISIONCLIENT;
        CrestronString URLPATH;
        object POLL_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                
                __context__.SourceCodeLine = 106;
                MYHIKVISIONCLIENT . GetInfo ( ) ; 
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    public override object FunctionMain (  object __obj__ ) 
        { 
        try
        {
            SplusExecutionContext __context__ = SplusFunctionMainStartCode();
            
            __context__.SourceCodeLine = 116;
            WaitForInitializationComplete ( ) ; 
            __context__.SourceCodeLine = 119;
            MYHIKVISIONCLIENT . Init ( IPADDRESS  .ToString(), (int)( PORT  .Value ), USERNAME  .ToString(), PASSWORD  .ToString()) ; 
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler(); }
        return __obj__;
        }
        
    
    public override void LogosSplusInitialize()
    {
        SocketInfo __socketinfo__ = new SocketInfo( 1, this );
        InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
        _SplusNVRAM = new SplusNVRAM( this );
        URLPATH  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        
        POLL = new Crestron.Logos.SplusObjects.DigitalInput( POLL__DigitalInput__, this );
        m_DigitalInputList.Add( POLL__DigitalInput__, POLL );
        
        PORT = new UShortParameter( PORT__Parameter__, this );
        m_ParameterList.Add( PORT__Parameter__, PORT );
        
        IPADDRESS = new StringParameter( IPADDRESS__Parameter__, this );
        m_ParameterList.Add( IPADDRESS__Parameter__, IPADDRESS );
        
        USERNAME = new StringParameter( USERNAME__Parameter__, this );
        m_ParameterList.Add( USERNAME__Parameter__, USERNAME );
        
        PASSWORD = new StringParameter( PASSWORD__Parameter__, this );
        m_ParameterList.Add( PASSWORD__Parameter__, PASSWORD );
        
        
        POLL.OnDigitalPush.Add( new InputChangeHandlerWrapper( POLL_OnPush_0, false ) );
        
        _SplusNVRAM.PopulateCustomAttributeList( true );
        
        NVRAM = _SplusNVRAM;
        
    }
    
    public override void LogosSimplSharpInitialize()
    {
        MYHIKVISIONCLIENT  = new HikvisionLibrary.HikvisionClient();
        
        
    }
    
    public UserModuleClass_AVL_HIKVISION ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}
    
    
    
    
    const uint IPADDRESS__Parameter__ = 10;
    const uint PORT__Parameter__ = 11;
    const uint USERNAME__Parameter__ = 12;
    const uint PASSWORD__Parameter__ = 13;
    const uint POLL__DigitalInput__ = 0;
    
    [SplusStructAttribute(-1, true, false)]
    public class SplusNVRAM : SplusStructureBase
    {
    
        public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
        
        
    }
    
    SplusNVRAM _SplusNVRAM = null;
    
    public class __CEvent__ : CEvent
    {
        public __CEvent__() {}
        public void Close() { base.Close(); }
        public int Reset() { return base.Reset() ? 1 : 0; }
        public int Set() { return base.Set() ? 1 : 0; }
        public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
    }
    public class __CMutex__ : CMutex
    {
        public __CMutex__() {}
        public void Close() { base.Close(); }
        public void ReleaseMutex() { base.ReleaseMutex(); }
        public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
    }
     public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}
