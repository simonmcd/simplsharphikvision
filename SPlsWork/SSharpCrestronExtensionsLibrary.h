namespace System.Text;
        // class declarations
         class StringBuilderExtensions;
         class TextExtensions;
    static class StringBuilderExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class TextExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace System;
        // class declarations
         class StringSplitOptions;
         class ArrayEx;
         class CharExtensions;
         class CharEx;
         class StringExtensions;
         class StringEx;
         class ByteEx;
         class SByteEx;
         class Int16Ex;
         class Int32Ex;
         class Int64Ex;
         class UInt16Ex;
         class UInt32Ex;
         class UInt64Ex;
         class BooleanEx;
         class DateTimeEx;
         class DoubleEx;
         class SingleEx;
         class DecimalEx;
         class EnumExtensions;
    static class StringSplitOptions // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER RemoveEmptyEntries;
    };

    static class ArrayEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class CharExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class CharEx 
    {
        // class delegates

        // class events

        // class functions
        static STRING_FUNCTION ConvertFromUtf32 ( SIGNED_LONG_INTEGER utf32 );
        static SIGNED_LONG_INTEGER_FUNCTION ConvertToUtf32 ( STRING s , SIGNED_LONG_INTEGER index );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class StringExtensions 
    {
        // class delegates

        // class events

        // class functions
        static STRING_FUNCTION ToLowerInvariant ( STRING str );
        static STRING_FUNCTION ToUpperInvariant ( STRING str );
        static STRING_FUNCTION Remove ( STRING str , SIGNED_LONG_INTEGER startIndex );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class StringEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class ByteEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class SByteEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class Int16Ex 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class Int32Ex 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class Int64Ex 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class UInt16Ex 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class UInt32Ex 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class UInt64Ex 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class BooleanEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class DateTimeEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class DoubleEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class SingleEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class DecimalEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class EnumExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace Crestron.SimplSharp.CrestronIO;
        // class declarations
         class CrestronStreamExtensions;
    static class CrestronStreamExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace Crestron.SimplSharp;
        // class declarations
         class CrestronEnvironmentEx;
         class SpecialFolder;
         class DnsEx;
         class OperatingSystem;
         class CrestronEventExtensions;
         class AutoResetEvent;
         class ManualResetEvent;
         class CrestronTimerExtensions;
    static class CrestronEnvironmentEx 
    {
        // class delegates

        // class events

        // class functions
        static STRING_FUNCTION GetFolderPath ( SpecialFolder folder );
        static FUNCTION FailFast ( STRING message );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING CurrentDirectory[];
        STRING MachineName[];
        STRING NewLine[];
        OperatingSystem OSVersion;
        STRING StackTrace[];
        SIGNED_LONG_INTEGER TickCount;
    };

    static class SpecialFolder // enum
    {
        static SIGNED_LONG_INTEGER Programs;
        static SIGNED_LONG_INTEGER ApplicationData;
        static SIGNED_LONG_INTEGER CommonApplicationData;
    };

    static class DnsEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class CrestronEventExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class CrestronTimerExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace OpenNETCF;
        // class declarations
         class Enum2;
    static class Enum2 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

