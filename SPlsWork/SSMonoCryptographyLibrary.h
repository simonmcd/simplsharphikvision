namespace SSMono.Security.Cryptography;
        // class declarations
         class AsymmetricKeyExchangeFormatter;
         class RSAOAEPKeyExchangeFormatter;
         class DeriveBytes;
         class Rfc2898DeriveBytes;
         class CryptoConvert;
         class KeyNumber;
         class SymmetricAlgorithm;
         class DES;
         class RSAParameters;
         class AsymmetricAlgorithm;
         class RSA;
         class RSAManaged;
         class HashAlgorithm;
         class MD5;
         class DSA;
         class AsymmetricSignatureDeformatter;
         class Rijndael;
         class RijndaelManaged;
         class RandomNumberGenerator;
         class KeyedHashAlgorithm;
         class CspProviderFlags;
         class CryptographicException;
         class CryptographicUnexpectedOperationException;
         class DESCryptoServiceProvider;
         class AsymmetricKeyExchangeDeformatter;
         class RSAOAEPKeyExchangeDeformatter;
         class RSACryptoServiceProvider;
         class HMAC;
         class HMACSHA512;
         class Aes;
         class TripleDES;
         class TripleDESCryptoServiceProvider;
         class SignatureDescription;
         class SHA1;
         class SHA1CryptoServiceProvider;
         class AsymmetricSignatureFormatter;
         class RSAPKCS1SignatureFormatter;
         class RSAPKCS1SignatureDeformatter;
         class RSAPKCS1KeyExchangeFormatter;
         class PKCS1;
         class CspParameters;
         class MACTripleDES;
         class DSASignatureDeformatter;
         class CryptoAPITransform;
         class PasswordDeriveBytes;
         class CryptoStream;
         class RC2;
         class RC2CryptoServiceProvider;
         class HMACSHA256;
         class HMACSHA1;
         class KeyBuilder;
         class BlockProcessor;
         class CryptoStreamMode;
         class CryptoConfig;
         class SHA512;
         class SHA384;
         class SHA384Managed;
         class SHA1Managed;
         class HMACMD5;
         class CipherMode;
         class DSACryptoServiceProvider;
         class CspKeyContainerInfo;
         class SHA256;
         class SHA256Managed;
         class HMACSHA384;
         class AesManaged;
         class RNGCryptoServiceProvider;
         class SHA512Managed;
         class RSAPKCS1KeyExchangeDeformatter;
         class KeySizes;
         class DSASignatureFormatter;
         class ToBase64Transform;
         class PaddingMode;
         class MD5CryptoServiceProvider;
         class FromBase64TransformMode;
         class FromBase64Transform;
         class DSAParameters;
         class RijndaelManagedTransform;
         class KeyPairPersistence;
         class AesCryptoServiceProvider;
           class CryptoKeySecurity;
           class SecureString;
     class AsymmetricKeyExchangeFormatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Parameters[];
    };

     class RSAOAEPKeyExchangeFormatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Parameters[];
        RandomNumberGenerator Rng;
    };

     class DeriveBytes 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class CryptoConvert 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class KeyNumber // enum
    {
        static SIGNED_LONG_INTEGER Exchange;
        static SIGNED_LONG_INTEGER Signature;
    };

     class SymmetricAlgorithm 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class DES 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class RSAParameters 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class AsymmetricAlgorithm 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION FromXmlString ( STRING xmlString );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING KeyExchangeAlgorithm[];
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalKeySizes[];
        STRING SignatureAlgorithm[];
    };

     class RSA 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION ImportParameters ( RSAParameters parameters );
        FUNCTION FromXmlString ( STRING xmlString );
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING KeyExchangeAlgorithm[];
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalKeySizes[];
        STRING SignatureAlgorithm[];
    };

     class RSAManaged 
    {
        // class delegates

        // class events
        EventHandler KeyGenerated ( RSAManaged sender, EventArgs e );

        // class functions
        FUNCTION ImportParameters ( RSAParameters parameters );
        FUNCTION FromXmlString ( STRING xmlString );
        FUNCTION set_KeySize ( SIGNED_LONG_INTEGER value );
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER KeySize;
        STRING KeyExchangeAlgorithm[];
        STRING SignatureAlgorithm[];
        KeySizes LegalKeySizes[];
    };

     class HashAlgorithm 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Initialize ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class MD5 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Initialize ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class DSA 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION FromXmlString ( STRING xmlString );
        FUNCTION ImportParameters ( DSAParameters parameters );
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING KeyExchangeAlgorithm[];
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalKeySizes[];
        STRING SignatureAlgorithm[];
    };

     class AsymmetricSignatureDeformatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetHashAlgorithm ( STRING strName );
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class Rijndael 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class RijndaelManaged 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class RandomNumberGenerator 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class KeyedHashAlgorithm 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Initialize ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

    static class CspProviderFlags // enum
    {
        static SIGNED_LONG_INTEGER NoFlags;
        static SIGNED_LONG_INTEGER UseMachineKeyStore;
        static SIGNED_LONG_INTEGER UseDefaultKeyContainer;
        static SIGNED_LONG_INTEGER UseNonExportableKey;
        static SIGNED_LONG_INTEGER UseExistingKey;
        static SIGNED_LONG_INTEGER UseArchivableKey;
        static SIGNED_LONG_INTEGER UseUserProtectedKey;
        static SIGNED_LONG_INTEGER NoPrompt;
    };

     class CryptographicException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

     class CryptographicUnexpectedOperationException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

     class DESCryptoServiceProvider 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class AsymmetricKeyExchangeDeformatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Parameters[];
    };

     class RSAOAEPKeyExchangeDeformatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Parameters[];
    };

     class RSACryptoServiceProvider 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION ImportParameters ( RSAParameters parameters );
        FUNCTION FromXmlString ( STRING xmlString );
        FUNCTION set_KeySize ( SIGNED_LONG_INTEGER value );
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING KeyExchangeAlgorithm[];
        SIGNED_LONG_INTEGER KeySize;
        STRING SignatureAlgorithm[];
        CspKeyContainerInfo CspKeyContainerInfo;
        KeySizes LegalKeySizes[];
    };

     class HMAC 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING HashName[];
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class HMACSHA512 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING HashName[];
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class Aes 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class TripleDES 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class TripleDESCryptoServiceProvider 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        SIGNED_LONG_INTEGER KeySize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class SignatureDescription 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING DeformatterAlgorithm[];
        STRING DigestAlgorithm[];
        STRING FormatterAlgorithm[];
        STRING KeyAlgorithm[];
    };

     class SHA1 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Initialize ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class SHA1CryptoServiceProvider 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class AsymmetricSignatureFormatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetHashAlgorithm ( STRING strName );
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class RSAPKCS1SignatureFormatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetHashAlgorithm ( STRING strName );
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class RSAPKCS1SignatureDeformatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetHashAlgorithm ( STRING strName );
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class RSAPKCS1KeyExchangeFormatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        RandomNumberGenerator Rng;
        STRING Parameters[];
    };

     class PKCS1 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class CspParameters 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        STRING KeyContainerName[];
        SIGNED_LONG_INTEGER KeyNumber;
        STRING ProviderName[];
        SIGNED_LONG_INTEGER ProviderType;

        // class properties
        CspProviderFlags Flags;
        CryptoKeySecurity CryptoKeySecurity;
        SecureString KeyPassword;
    };

     class MACTripleDES 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        PaddingMode Padding;
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class DSASignatureDeformatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetHashAlgorithm ( STRING strName );
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class CryptoAPITransform 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class RC2 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER EffectiveKeySize;
        SIGNED_LONG_INTEGER KeySize;
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class RC2CryptoServiceProvider 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER EffectiveKeySize;
        SIGNED_LONG_INTEGER KeySize;
        SIGNED_LONG_INTEGER BlockSize;
        SIGNED_LONG_INTEGER FeedbackSize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
        CipherMode Mode;
        PaddingMode Padding;
    };

     class HMACSHA256 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING HashName[];
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class HMACSHA1 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING HashName[];
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class KeyBuilder 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class CryptoStreamMode // enum
    {
        static SIGNED_LONG_INTEGER Read;
        static SIGNED_LONG_INTEGER Write;
    };

     class CryptoConfig 
    {
        // class delegates

        // class events

        // class functions
        static STRING_FUNCTION MapNameToOID ( STRING name );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class SHA512 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Initialize ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class SHA384 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Initialize ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class SHA384Managed 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class SHA1Managed 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class HMACMD5 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING HashName[];
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

    static class CipherMode // enum
    {
        static SIGNED_LONG_INTEGER CBC;
        static SIGNED_LONG_INTEGER ECB;
        static SIGNED_LONG_INTEGER OFB;
        static SIGNED_LONG_INTEGER CFB;
        static SIGNED_LONG_INTEGER CTS;
    };

     class DSACryptoServiceProvider 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION ImportParameters ( DSAParameters parameters );
        FUNCTION FromXmlString ( STRING xmlString );
        FUNCTION set_KeySize ( SIGNED_LONG_INTEGER value );
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING KeyExchangeAlgorithm[];
        SIGNED_LONG_INTEGER KeySize;
        STRING SignatureAlgorithm[];
        CspKeyContainerInfo CspKeyContainerInfo;
        KeySizes LegalKeySizes[];
    };

     class SHA256 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Initialize ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class SHA256Managed 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class HMACSHA384 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING HashName[];
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class AesManaged 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER KeySize;
        SIGNED_LONG_INTEGER FeedbackSize;
        CipherMode Mode;
        PaddingMode Padding;
        SIGNED_LONG_INTEGER BlockSize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
    };

     class RNGCryptoServiceProvider 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class SHA512Managed 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class RSAPKCS1KeyExchangeDeformatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Parameters[];
        RandomNumberGenerator RNG;
    };

     class DSASignatureFormatter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION SetHashAlgorithm ( STRING strName );
        FUNCTION SetKey ( AsymmetricAlgorithm key );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class ToBase64Transform 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Dispose ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

    static class PaddingMode // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER PKCS7;
        static SIGNED_LONG_INTEGER Zeros;
        static SIGNED_LONG_INTEGER ANSIX923;
        static SIGNED_LONG_INTEGER ISO10126;
    };

     class MD5CryptoServiceProvider 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Initialize ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER HashSize;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

    static class FromBase64TransformMode // enum
    {
        static SIGNED_LONG_INTEGER IgnoreWhiteSpaces;
        static SIGNED_LONG_INTEGER DoNotIgnoreWhiteSpaces;
    };

     class FromBase64Transform 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class DSAParameters 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        SIGNED_LONG_INTEGER Counter;

        // class properties
    };

     class RijndaelManagedTransform 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BlockSizeValue;
        SIGNED_LONG_INTEGER InputBlockSize;
        SIGNED_LONG_INTEGER OutputBlockSize;
    };

     class AesCryptoServiceProvider 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION GenerateIV ();
        FUNCTION GenerateKey ();
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER KeySize;
        SIGNED_LONG_INTEGER FeedbackSize;
        CipherMode Mode;
        PaddingMode Padding;
        SIGNED_LONG_INTEGER BlockSize;
        KeySizes LegalBlockSizes[];
        KeySizes LegalKeySizes[];
    };

namespace Mono.Security.Cryptography;
        // class declarations
         class DSAManaged;
     class DSAManaged 
    {
        // class delegates

        // class events
        EventHandler KeyGenerated ( DSAManaged sender, EventArgs e );

        // class functions
        FUNCTION ImportParameters ( DSAParameters parameters );
        FUNCTION FromXmlString ( STRING xmlString );
        FUNCTION set_KeySize ( SIGNED_LONG_INTEGER value );
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER KeySize;
        STRING KeyExchangeAlgorithm[];
        STRING SignatureAlgorithm[];
        KeySizes LegalKeySizes;
    };

namespace SSMono.Xml;
        // class declarations
         class MiniParser;
         class HandlerAdapter;
         class AttrListImpl;
         class XMLError;
         class SecurityParser;
     class MiniParser 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class HandlerAdapter 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION OnStartParsing ( MiniParser parser );
        FUNCTION OnEndElement ( STRING name );
        FUNCTION OnChars ( STRING ch );
        FUNCTION OnEndParsing ( MiniParser parser );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class AttrListImpl 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION GetName ( SIGNED_LONG_INTEGER i );
        STRING_FUNCTION GetValue ( SIGNED_LONG_INTEGER i );
        FUNCTION ChangeValue ( STRING name , STRING newValue );
        FUNCTION Clear ();
        FUNCTION Add ( STRING name , STRING value );
        FUNCTION Remove ( SIGNED_LONG_INTEGER i );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Length;
        STRING Names[][];
        STRING Values[][];
    };

     class XMLError 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Line;
        SIGNED_LONG_INTEGER Column;
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

     class SecurityParser 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION LoadXml ( STRING xml );
        SIGNED_LONG_INTEGER_FUNCTION Read ();
        FUNCTION OnStartParsing ( MiniParser parser );
        FUNCTION OnEndElement ( STRING name );
        FUNCTION OnChars ( STRING ch );
        FUNCTION OnEndParsing ( MiniParser parser );
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace SSMono.Security.AccessControl;
        // class declarations
         class CryptoKeySecurity;
     class CryptoKeySecurity 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace Mono.Math;
        // class declarations
         class BigInteger;
         class Sign;
         class ModulusRing;
     class BigInteger 
    {
        // class delegates

        // class events

        // class functions
        static SIGNED_LONG_INTEGER_FUNCTION op_Modulus ( BigInteger bi , SIGNED_LONG_INTEGER i );
        static SIGNED_LONG_INTEGER_FUNCTION Modulus ( BigInteger bi , SIGNED_LONG_INTEGER i );
        FUNCTION Randomize ( RandomNumberGenerator rng );
        SIGNED_LONG_INTEGER_FUNCTION BitCount ();
        FUNCTION SetBit ( LONG_INTEGER bitNum );
        FUNCTION ClearBit ( LONG_INTEGER bitNum );
        SIGNED_LONG_INTEGER_FUNCTION LowestSetBit ();
        STRING_FUNCTION ToString ( LONG_INTEGER radix );
        FUNCTION Clear ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        FUNCTION Incr2 ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class Sign // enum
    {
        static SIGNED_LONG_INTEGER Zero;
        static SIGNED_LONG_INTEGER Positive;
        static SIGNED_LONG_INTEGER Negative;
    };

namespace SSMono.Security;
        // class declarations
         class SecureString;
     class SecureString 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Dispose ();
        FUNCTION MakeReadOnly ();
        FUNCTION RemoveAt ( SIGNED_LONG_INTEGER index );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Length;
    };

namespace Mono.Math.Prime;
        // class declarations
         class PrimalityTests;
         class ConfidenceFactor;
     class PrimalityTests 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class ConfidenceFactor // enum
    {
        static SIGNED_LONG_INTEGER ExtraLow;
        static SIGNED_LONG_INTEGER Low;
        static SIGNED_LONG_INTEGER Medium;
        static SIGNED_LONG_INTEGER High;
        static SIGNED_LONG_INTEGER ExtraHigh;
        static SIGNED_LONG_INTEGER Provable;
    };

namespace Mono.Security;
        // class declarations
         class ASN1Convert;
         class ASN1;
    static class ASN1Convert 
    {
        // class delegates

        // class events

        // class functions
        static SIGNED_LONG_INTEGER_FUNCTION ToInt32 ( ASN1 asn1 );
        static STRING_FUNCTION ToOid ( ASN1 asn1 );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class ASN1 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        FUNCTION SaveToFile ( STRING filename );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Count;
        SIGNED_LONG_INTEGER Length;
    };

namespace Mono.Math.Prime.Generator;
        // class declarations
         class PrimeGeneratorBase;
         class SequentialSearchPrimeGeneratorBase;
         class NextPrimeFinder;
           class PrimalityTest;
     class PrimeGeneratorBase 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        ConfidenceFactor Confidence;
        SIGNED_LONG_INTEGER TrialDivisionBounds;
    };

     class SequentialSearchPrimeGeneratorBase 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        ConfidenceFactor Confidence;
        SIGNED_LONG_INTEGER TrialDivisionBounds;
    };

     class NextPrimeFinder 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        ConfidenceFactor Confidence;
        SIGNED_LONG_INTEGER TrialDivisionBounds;
    };

