namespace SimplSharpHTTPLibrary;
        // class declarations
         class SimplSharpHTTPClient;
         class SimplSharpHTTPServer;
         class SimplSharpWebClient;
     class SimplSharpHTTPClient 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION ClientConnect ( STRING addressToConnectTo , SIGNED_LONG_INTEGER port );
        STRING_FUNCTION SendGet ( STRING urlPath );
        FUNCTION SendPost ( STRING urlPath );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class SimplSharpHTTPServer 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION InitializeHTTPServer ( STRING addressToAcceptConnectionFrom , SIGNED_LONG_INTEGER portSent );
        FUNCTION StartServer ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class SimplSharpWebClient 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION SendGet ( STRING ipAddress , SIGNED_LONG_INTEGER port , STRING urlPath , STRING username , STRING password );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

