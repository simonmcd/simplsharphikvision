namespace HikvisionLibrary;
        // class declarations
         class HikvisionClient;
     class HikvisionClient 
    {
        // class delegates
        delegate FUNCTION OutputSensorStateDel ( INTEGER enabled , SIMPLSHARPSTRING detection , INTEGER lowLight , SIMPLSHARPSTRING walkTest , INTEGER tempValue , SIMPLSHARPSTRING tempUnit , SIMPLSHARPSTRING digitalOutput1 , SIMPLSHARPSTRING digitalOutput2 , SIMPLSHARPSTRING tamper );

        // class events

        // class functions
        FUNCTION Init ( STRING initAddress , SIGNED_LONG_INTEGER initPort , STRING initUsername , STRING initPassword );
        FUNCTION GetInfo ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        DelegateProperty OutputSensorStateDel OutputSensorState;
    };

