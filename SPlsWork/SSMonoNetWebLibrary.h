namespace SSMono.Net;
        // class declarations
         class WebExceptionStatus;
         class ServicePointManager;
         class ChainValidationHelper;
         class HttpResponseHeader;
         class HttpVersion;
         class WebRequest;
         class FileWebRequest;
         class FileWebStream;
         class SecurityProtocolType;
         class HttpListenerBasicIdentity;
         class ProtocolViolationException;
         class HttpListenerContext;
         class CredentialCache;
         class DecompressionMethods;
         class WebHeaderCollection;
         class HttpListenerResponse;
         class HttpListener;
         class WebException;
         class SocketAsyncOperation;
         class HttpStatusCode;
         class FtpStatusCode;
         class HttpWebRequest;
         class HttpListenerRequest;
         class HttpListenerException;
         class WebResponse;
         class FtpWebResponse;
         class FtpWebRequest;
         class HttpRequestHeader;
         class HttpWebResponse;
         class FileWebResponse;
         class HttpListenerPrefixCollection;
         class AuthenticationSchemes;
         class AuthenticationManager;
         class Authorization;
         class ServicePoint;
         class WebRequestMethods;
         class File;
         class Ftp;
         class Http;
         class WebProxy;
         class GlobalProxySelection;
         class EmptyWebProxy;
           class AuthenticationLevel;
           class RequestCachePolicy;
           class TokenImpersonationLevel;
    static class WebExceptionStatus // enum
    {
        static SIGNED_LONG_INTEGER Success;
        static SIGNED_LONG_INTEGER NameResolutionFailure;
        static SIGNED_LONG_INTEGER ConnectFailure;
        static SIGNED_LONG_INTEGER ReceiveFailure;
        static SIGNED_LONG_INTEGER SendFailure;
        static SIGNED_LONG_INTEGER PipelineFailure;
        static SIGNED_LONG_INTEGER RequestCanceled;
        static SIGNED_LONG_INTEGER ProtocolError;
        static SIGNED_LONG_INTEGER ConnectionClosed;
        static SIGNED_LONG_INTEGER TrustFailure;
        static SIGNED_LONG_INTEGER SecureChannelFailure;
        static SIGNED_LONG_INTEGER ServerProtocolViolation;
        static SIGNED_LONG_INTEGER KeepAliveFailure;
        static SIGNED_LONG_INTEGER Pending;
        static SIGNED_LONG_INTEGER Timeout;
        static SIGNED_LONG_INTEGER ProxyNameResolutionFailure;
        static SIGNED_LONG_INTEGER UnknownError;
        static SIGNED_LONG_INTEGER MessageLengthLimitExceeded;
        static SIGNED_LONG_INTEGER CacheEntryNotFound;
        static SIGNED_LONG_INTEGER RequestProhibitedByCachePolicy;
        static SIGNED_LONG_INTEGER RequestProhibitedByProxy;
    };

     class ServicePointManager 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static SIGNED_LONG_INTEGER DefaultNonPersistentConnectionLimit;
        static SIGNED_LONG_INTEGER DefaultPersistentConnectionLimit;

        // class properties
        SIGNED_LONG_INTEGER DefaultConnectionLimit;
        SIGNED_LONG_INTEGER DnsRefreshTimeout;
        SIGNED_LONG_INTEGER MaxServicePointIdleTime;
        SIGNED_LONG_INTEGER MaxServicePoints;
        SecurityProtocolType SecurityProtocol;
    };

    static class HttpResponseHeader // enum
    {
        static SIGNED_LONG_INTEGER CacheControl;
        static SIGNED_LONG_INTEGER Connection;
        static SIGNED_LONG_INTEGER Date;
        static SIGNED_LONG_INTEGER KeepAlive;
        static SIGNED_LONG_INTEGER Pragma;
        static SIGNED_LONG_INTEGER Trailer;
        static SIGNED_LONG_INTEGER TransferEncoding;
        static SIGNED_LONG_INTEGER Upgrade;
        static SIGNED_LONG_INTEGER Via;
        static SIGNED_LONG_INTEGER Warning;
        static SIGNED_LONG_INTEGER Allow;
        static SIGNED_LONG_INTEGER ContentLength;
        static SIGNED_LONG_INTEGER ContentType;
        static SIGNED_LONG_INTEGER ContentEncoding;
        static SIGNED_LONG_INTEGER ContentLanguage;
        static SIGNED_LONG_INTEGER ContentLocation;
        static SIGNED_LONG_INTEGER ContentMd5;
        static SIGNED_LONG_INTEGER ContentRange;
        static SIGNED_LONG_INTEGER Expires;
        static SIGNED_LONG_INTEGER LastModified;
        static SIGNED_LONG_INTEGER AcceptRanges;
        static SIGNED_LONG_INTEGER Age;
        static SIGNED_LONG_INTEGER ETag;
        static SIGNED_LONG_INTEGER Location;
        static SIGNED_LONG_INTEGER ProxyAuthenticate;
        static SIGNED_LONG_INTEGER RetryAfter;
        static SIGNED_LONG_INTEGER Server;
        static SIGNED_LONG_INTEGER SetCookie;
        static SIGNED_LONG_INTEGER Vary;
        static SIGNED_LONG_INTEGER WwwAuthenticate;
    };

     class HttpVersion 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class WebRequest 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Abort ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        AuthenticationLevel AuthenticationLevel;
        STRING ConnectionGroupName[];
        STRING ContentType[];
        RequestCachePolicy CachePolicy;
        RequestCachePolicy DefaultCachePolicy;
        WebHeaderCollection Headers;
        STRING Method[];
        SIGNED_LONG_INTEGER Timeout;
        TokenImpersonationLevel ImpersonationLevel;
    };

     class FileWebRequest 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Abort ();
        FUNCTION set_Headers ( WebHeaderCollection value );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING ConnectionGroupName[];
        STRING ContentType[];
        WebHeaderCollection Headers;
        STRING Method[];
        SIGNED_LONG_INTEGER Timeout;
        AuthenticationLevel AuthenticationLevel;
        RequestCachePolicy CachePolicy;
        RequestCachePolicy DefaultCachePolicy;
        TokenImpersonationLevel ImpersonationLevel;
    };

     class FileWebStream 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION Flush ();
        SIGNED_LONG_INTEGER_FUNCTION ReadByte ();
        FUNCTION Dispose ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        SIGNED_LONG_INTEGER ReadTimeout;
        SIGNED_LONG_INTEGER WriteTimeout;
    };

    static class SecurityProtocolType // enum
    {
        static SIGNED_LONG_INTEGER Ssl3;
        static SIGNED_LONG_INTEGER Tls;
    };

     class ProtocolViolationException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

     class HttpListenerContext 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        HttpListenerRequest Request;
        HttpListenerResponse Response;
    };

     class CredentialCache 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Remove ( STRING host , SIGNED_LONG_INTEGER port , STRING authenticationType );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class DecompressionMethods // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER GZip;
        static SIGNED_LONG_INTEGER Deflate;
    };

     class WebHeaderCollection 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Add ( STRING header );
        FUNCTION Remove ( STRING name );
        FUNCTION Set ( STRING name , STRING value );
        STRING_FUNCTION ToString ();
        STRING_FUNCTION Get ( SIGNED_LONG_INTEGER index );
        STRING_FUNCTION GetKey ( SIGNED_LONG_INTEGER index );
        STRING_FUNCTION get_Item ( HttpRequestHeader header );
        FUNCTION set_Item ( HttpRequestHeader header , STRING value );
        FUNCTION Clear ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING AllKeys[][];
        SIGNED_LONG_INTEGER Count;
    };

     class HttpListenerResponse 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Abort ();
        FUNCTION AddHeader ( STRING name , STRING value );
        FUNCTION AppendHeader ( STRING name , STRING value );
        FUNCTION Close ();
        FUNCTION CopyFrom ( HttpListenerResponse templateResponse );
        FUNCTION Redirect ( STRING url );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING ContentType[];
        WebHeaderCollection Headers;
        STRING RedirectLocation[];
        SIGNED_LONG_INTEGER StatusCode;
        STRING StatusDescription[];
    };

     class HttpListener 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Abort ();
        FUNCTION Close ();
        FUNCTION Start ();
        FUNCTION Stop ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        AuthenticationSchemes AuthenticationSchemes;
        HttpListenerPrefixCollection Prefixes;
        STRING Realm[];
    };

     class WebException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        WebResponse Response;
        WebExceptionStatus Status;
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

    static class SocketAsyncOperation // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Accept;
        static SIGNED_LONG_INTEGER Connect;
        static SIGNED_LONG_INTEGER Disconnect;
        static SIGNED_LONG_INTEGER Receive;
        static SIGNED_LONG_INTEGER ReceiveFrom;
        static SIGNED_LONG_INTEGER ReceiveMessageFrom;
        static SIGNED_LONG_INTEGER Send;
        static SIGNED_LONG_INTEGER SendPackets;
        static SIGNED_LONG_INTEGER SendTo;
    };

    static class HttpStatusCode // enum
    {
        static SIGNED_LONG_INTEGER Continue;
        static SIGNED_LONG_INTEGER SwitchingProtocols;
        static SIGNED_LONG_INTEGER OK;
        static SIGNED_LONG_INTEGER Created;
        static SIGNED_LONG_INTEGER Accepted;
        static SIGNED_LONG_INTEGER NonAuthoritativeInformation;
        static SIGNED_LONG_INTEGER NoContent;
        static SIGNED_LONG_INTEGER ResetContent;
        static SIGNED_LONG_INTEGER PartialContent;
        static SIGNED_LONG_INTEGER Ambiguous;
        static SIGNED_LONG_INTEGER MovedPermanently;
        static SIGNED_LONG_INTEGER Redirect;
        static SIGNED_LONG_INTEGER SeeOther;
        static SIGNED_LONG_INTEGER NotModified;
        static SIGNED_LONG_INTEGER UseProxy;
        static SIGNED_LONG_INTEGER Unused;
        static SIGNED_LONG_INTEGER TemporaryRedirect;
        static SIGNED_LONG_INTEGER BadRequest;
        static SIGNED_LONG_INTEGER Unauthorized;
        static SIGNED_LONG_INTEGER PaymentRequired;
        static SIGNED_LONG_INTEGER Forbidden;
        static SIGNED_LONG_INTEGER NotFound;
        static SIGNED_LONG_INTEGER MethodNotAllowed;
        static SIGNED_LONG_INTEGER NotAcceptable;
        static SIGNED_LONG_INTEGER ProxyAuthenticationRequired;
        static SIGNED_LONG_INTEGER RequestTimeout;
        static SIGNED_LONG_INTEGER Conflict;
        static SIGNED_LONG_INTEGER Gone;
        static SIGNED_LONG_INTEGER LengthRequired;
        static SIGNED_LONG_INTEGER PreconditionFailed;
        static SIGNED_LONG_INTEGER RequestEntityTooLarge;
        static SIGNED_LONG_INTEGER RequestUriTooLong;
        static SIGNED_LONG_INTEGER UnsupportedMediaType;
        static SIGNED_LONG_INTEGER RequestedRangeNotSatisfiable;
        static SIGNED_LONG_INTEGER ExpectationFailed;
        static SIGNED_LONG_INTEGER InternalServerError;
        static SIGNED_LONG_INTEGER NotImplemented;
        static SIGNED_LONG_INTEGER BadGateway;
        static SIGNED_LONG_INTEGER ServiceUnavailable;
        static SIGNED_LONG_INTEGER GatewayTimeout;
        static SIGNED_LONG_INTEGER HttpVersionNotSupported;
    };

    static class FtpStatusCode // enum
    {
        static SIGNED_LONG_INTEGER Undefined;
        static SIGNED_LONG_INTEGER RestartMarker;
        static SIGNED_LONG_INTEGER ServiceTemporarilyNotAvailable;
        static SIGNED_LONG_INTEGER DataAlreadyOpen;
        static SIGNED_LONG_INTEGER OpeningData;
        static SIGNED_LONG_INTEGER CommandOK;
        static SIGNED_LONG_INTEGER CommandExtraneous;
        static SIGNED_LONG_INTEGER DirectoryStatus;
        static SIGNED_LONG_INTEGER FileStatus;
        static SIGNED_LONG_INTEGER SystemType;
        static SIGNED_LONG_INTEGER SendUserCommand;
        static SIGNED_LONG_INTEGER ClosingControl;
        static SIGNED_LONG_INTEGER ClosingData;
        static SIGNED_LONG_INTEGER EnteringPassive;
        static SIGNED_LONG_INTEGER LoggedInProceed;
        static SIGNED_LONG_INTEGER ServerWantsSecureSession;
        static SIGNED_LONG_INTEGER FileActionOK;
        static SIGNED_LONG_INTEGER PathnameCreated;
        static SIGNED_LONG_INTEGER SendPasswordCommand;
        static SIGNED_LONG_INTEGER NeedLoginAccount;
        static SIGNED_LONG_INTEGER FileCommandPending;
        static SIGNED_LONG_INTEGER ServiceNotAvailable;
        static SIGNED_LONG_INTEGER CantOpenData;
        static SIGNED_LONG_INTEGER ConnectionClosed;
        static SIGNED_LONG_INTEGER ActionNotTakenFileUnavailableOrBusy;
        static SIGNED_LONG_INTEGER ActionAbortedLocalProcessingError;
        static SIGNED_LONG_INTEGER ActionNotTakenInsufficientSpace;
        static SIGNED_LONG_INTEGER CommandSyntaxError;
        static SIGNED_LONG_INTEGER ArgumentSyntaxError;
        static SIGNED_LONG_INTEGER CommandNotImplemented;
        static SIGNED_LONG_INTEGER BadCommandSequence;
        static SIGNED_LONG_INTEGER NotLoggedIn;
        static SIGNED_LONG_INTEGER AccountNeeded;
        static SIGNED_LONG_INTEGER ActionNotTakenFileUnavailable;
        static SIGNED_LONG_INTEGER ActionAbortedUnknownPageType;
        static SIGNED_LONG_INTEGER FileActionAborted;
        static SIGNED_LONG_INTEGER ActionNotTakenFilenameNotAllowed;
    };

     class HttpWebRequest 
    {
        // class delegates
        delegate FUNCTION HttpContinueDelegate ( SIGNED_LONG_INTEGER StatusCode , WebHeaderCollection httpHeaders );

        // class events

        // class functions
        FUNCTION AddRange ( SIGNED_LONG_INTEGER range );
        FUNCTION Abort ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Accept[];
        DecompressionMethods AutomaticDecompression;
        STRING Connection[];
        STRING ConnectionGroupName[];
        STRING ContentType[];
        DelegateProperty HttpContinueDelegate ContinueDelegate;
        RequestCachePolicy DefaultCachePolicy;
        SIGNED_LONG_INTEGER DefaultMaximumErrorResponseLength;
        STRING Expect[];
        WebHeaderCollection Headers;
        STRING Host[];
        SIGNED_LONG_INTEGER MaximumAutomaticRedirections;
        SIGNED_LONG_INTEGER MaximumResponseHeadersLength;
        SIGNED_LONG_INTEGER DefaultMaximumResponseHeadersLength;
        SIGNED_LONG_INTEGER ReadWriteTimeout;
        STRING MediaType[];
        STRING Method[];
        STRING Referer[];
        ServicePoint ServicePoint;
        SIGNED_LONG_INTEGER Timeout;
        STRING TransferEncoding[];
        STRING UserAgent[];
        AuthenticationLevel AuthenticationLevel;
        RequestCachePolicy CachePolicy;
        TokenImpersonationLevel ImpersonationLevel;
    };

     class HttpListenerRequest 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING AcceptTypes[][];
        SIGNED_LONG_INTEGER ClientCertificateError;
        STRING ContentType[];
        STRING HttpMethod[];
        STRING RawUrl[];
        STRING UserAgent[];
        STRING UserHostAddress[];
        STRING UserHostName[];
        STRING UserLanguages[][];
        STRING ServiceName[];
    };

     class HttpListenerException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER ErrorCode;
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

     class WebResponse 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION Dispose ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING ContentType[];
        WebHeaderCollection Headers;
    };

     class FtpWebResponse 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION Dispose ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        WebHeaderCollection Headers;
        STRING BannerMessage[];
        STRING WelcomeMessage[];
        STRING ExitMessage[];
        FtpStatusCode StatusCode;
        STRING StatusDescription[];
        STRING ContentType[];
    };

     class FtpWebRequest 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Abort ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING ConnectionGroupName[];
        STRING ContentType[];
        RequestCachePolicy DefaultCachePolicy;
        WebHeaderCollection Headers;
        STRING Method[];
        SIGNED_LONG_INTEGER ReadWriteTimeout;
        STRING RenameTo[];
        ServicePoint ServicePoint;
        SIGNED_LONG_INTEGER Timeout;
        AuthenticationLevel AuthenticationLevel;
        RequestCachePolicy CachePolicy;
        TokenImpersonationLevel ImpersonationLevel;
    };

    static class HttpRequestHeader // enum
    {
        static SIGNED_LONG_INTEGER CacheControl;
        static SIGNED_LONG_INTEGER Connection;
        static SIGNED_LONG_INTEGER Date;
        static SIGNED_LONG_INTEGER KeepAlive;
        static SIGNED_LONG_INTEGER Pragma;
        static SIGNED_LONG_INTEGER Trailer;
        static SIGNED_LONG_INTEGER TransferEncoding;
        static SIGNED_LONG_INTEGER Upgrade;
        static SIGNED_LONG_INTEGER Via;
        static SIGNED_LONG_INTEGER Warning;
        static SIGNED_LONG_INTEGER Allow;
        static SIGNED_LONG_INTEGER ContentLength;
        static SIGNED_LONG_INTEGER ContentType;
        static SIGNED_LONG_INTEGER ContentEncoding;
        static SIGNED_LONG_INTEGER ContentLanguage;
        static SIGNED_LONG_INTEGER ContentLocation;
        static SIGNED_LONG_INTEGER ContentMd5;
        static SIGNED_LONG_INTEGER ContentRange;
        static SIGNED_LONG_INTEGER Expires;
        static SIGNED_LONG_INTEGER LastModified;
        static SIGNED_LONG_INTEGER Accept;
        static SIGNED_LONG_INTEGER AcceptCharset;
        static SIGNED_LONG_INTEGER AcceptEncoding;
        static SIGNED_LONG_INTEGER AcceptLanguage;
        static SIGNED_LONG_INTEGER Authorization;
        static SIGNED_LONG_INTEGER Cookie;
        static SIGNED_LONG_INTEGER Expect;
        static SIGNED_LONG_INTEGER From;
        static SIGNED_LONG_INTEGER Host;
        static SIGNED_LONG_INTEGER IfMatch;
        static SIGNED_LONG_INTEGER IfModifiedSince;
        static SIGNED_LONG_INTEGER IfNoneMatch;
        static SIGNED_LONG_INTEGER IfRange;
        static SIGNED_LONG_INTEGER IfUnmodifiedSince;
        static SIGNED_LONG_INTEGER MaxForwards;
        static SIGNED_LONG_INTEGER ProxyAuthorization;
        static SIGNED_LONG_INTEGER Referer;
        static SIGNED_LONG_INTEGER Range;
        static SIGNED_LONG_INTEGER Te;
        static SIGNED_LONG_INTEGER Translate;
        static SIGNED_LONG_INTEGER UserAgent;
    };

     class HttpWebResponse 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION GetResponseHeader ( STRING headerName );
        FUNCTION Close ();
        FUNCTION set_ContentType ( STRING value );
        FUNCTION Dispose ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING CharacterSet[];
        STRING ContentEncoding[];
        STRING ContentType[];
        WebHeaderCollection Headers;
        STRING Method[];
        STRING Server[];
        HttpStatusCode StatusCode;
        STRING StatusDescription[];
    };

     class FileWebResponse 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION set_ContentType ( STRING value );
        FUNCTION Dispose ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING ContentType[];
        WebHeaderCollection Headers;
    };

     class HttpListenerPrefixCollection 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Add ( STRING uriPrefix );
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Count;
    };

    static class AuthenticationSchemes // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Digest;
        static SIGNED_LONG_INTEGER Negotiate;
        static SIGNED_LONG_INTEGER Ntlm;
        static SIGNED_LONG_INTEGER IntegratedWindowsAuthentication;
        static SIGNED_LONG_INTEGER Basic;
        static SIGNED_LONG_INTEGER Anonymous;
    };

     class AuthenticationManager 
    {
        // class delegates

        // class events

        // class functions
        static FUNCTION Unregister ( STRING authenticationScheme );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class ServicePoint 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER ConnectionLeaseTimeout;
        SIGNED_LONG_INTEGER ConnectionLimit;
        STRING ConnectionName[];
        SIGNED_LONG_INTEGER CurrentConnections;
        SIGNED_LONG_INTEGER MaxIdleTime;
        SIGNED_LONG_INTEGER ReceiveBufferSize;
    };

    static class WebRequestMethods 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class File 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING DownloadFile[];
        static STRING UploadFile[];

        // class properties
    };

    static class Ftp 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING AppendFile[];
        static STRING DeleteFile[];
        static STRING DownloadFile[];
        static STRING GetFileSize[];
        static STRING GetDateTimestamp[];
        static STRING ListDirectory[];
        static STRING ListDirectoryDetails[];
        static STRING MakeDirectory[];
        static STRING PrintWorkingDirectory[];
        static STRING RemoveDirectory[];
        static STRING Rename[];
        static STRING UploadFile[];
        static STRING UploadFileWithUniqueName[];

        // class properties
    };

    static class Http 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING Connect[];
        static STRING Get[];
        static STRING Head[];
        static STRING MkCol[];
        static STRING Post[];
        static STRING Put[];

        // class properties
    };

     class WebProxy 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING BypassList[][];
    };

     class GlobalProxySelection 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class EmptyWebProxy 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace SSMono.Net.Sockets;
        // class declarations
         class SocketAsyncEventArgs;
         class SendPacketsElement;
         class IPPacketInformation;
     class SocketAsyncEventArgs 
    {
        // class delegates

        // class events
        EventHandler Completed ( SocketAsyncEventArgs sender, SocketAsyncEventArgs e );

        // class functions
        FUNCTION Dispose ();
        FUNCTION SetBuffer ( SIGNED_LONG_INTEGER offset , SIGNED_LONG_INTEGER count );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER BytesTransferred;
        SIGNED_LONG_INTEGER Count;
        SocketAsyncOperation LastOperation;
        SIGNED_LONG_INTEGER Offset;
        IPPacketInformation ReceiveMessageFromPacketInfo;
        SendPacketsElement SendPacketsElements[];
        SIGNED_LONG_INTEGER SendPacketsSendSize;
    };

     class IPPacketInformation 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Interface;
    };

namespace SSMono.Net.Cache;
        // class declarations
         class HttpRequestCacheLevel;
         class HttpCacheAgeControl;
         class RequestCachePolicy;
         class HttpRequestCachePolicy;
         class RequestCacheLevel;
    static class HttpRequestCacheLevel // enum
    {
        static SIGNED_LONG_INTEGER Default;
        static SIGNED_LONG_INTEGER BypassCache;
        static SIGNED_LONG_INTEGER CacheOnly;
        static SIGNED_LONG_INTEGER CacheIfAvailable;
        static SIGNED_LONG_INTEGER Revalidate;
        static SIGNED_LONG_INTEGER Reload;
        static SIGNED_LONG_INTEGER NoCacheNoStore;
        static SIGNED_LONG_INTEGER CacheOrNextCacheOnly;
        static SIGNED_LONG_INTEGER Refresh;
    };

    static class HttpCacheAgeControl // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER MinFresh;
        static SIGNED_LONG_INTEGER MaxAge;
        static SIGNED_LONG_INTEGER MaxAgeAndMinFresh;
        static SIGNED_LONG_INTEGER MaxStale;
        static SIGNED_LONG_INTEGER MaxAgeAndMaxStale;
    };

     class RequestCachePolicy 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        RequestCacheLevel Level;
    };

     class HttpRequestCachePolicy 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        HttpRequestCacheLevel Level;
    };

    static class RequestCacheLevel // enum
    {
        static SIGNED_LONG_INTEGER Default;
        static SIGNED_LONG_INTEGER BypassCache;
        static SIGNED_LONG_INTEGER CacheOnly;
        static SIGNED_LONG_INTEGER CacheIfAvailable;
        static SIGNED_LONG_INTEGER Revalidate;
        static SIGNED_LONG_INTEGER Reload;
        static SIGNED_LONG_INTEGER NoCacheNoStore;
    };

namespace SSMono.Net.Security;
        // class declarations
         class TokenImpersonationLevel;
         class AuthenticationLevel;
    static class TokenImpersonationLevel // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Anonymous;
        static SIGNED_LONG_INTEGER Identification;
        static SIGNED_LONG_INTEGER Impersonation;
        static SIGNED_LONG_INTEGER Delegation;
    };

    static class AuthenticationLevel // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER MutualAuthRequested;
        static SIGNED_LONG_INTEGER MutualAuthRequired;
    };

