namespace SSMono.Security.Cryptography.X509Certificates;
        // class declarations
         class X509SubjectKeyIdentifierHashAlgorithm;
         class X509Extension;
         class X509SubjectKeyIdentifierExtension;
         class X509NameType;
         class X509KeyUsageExtension;
         class X509ChainStatusFlags;
         class X509Certificate;
         class X509Certificate2;
         class X509Certificate2Enumerator;
         class StoreName;
         class PublicKey;
         class X509KeyStorageFlags;
         class X509ChainPolicy;
         class X509RevocationFlag;
         class X509ExtensionCollection;
         class X509ChainElementEnumerator;
         class X500DistinguishedNameFlags;
         class OpenFlags;
         class X509FindType;
         class X509KeyUsageFlags;
         class X509Chain;
         class X509BasicConstraintsExtension;
         class X500DistinguishedName;
         class X509IncludeOption;
         class X509ContentType;
         class StoreLocation;
         class X509VerificationFlags;
         class X509ChainElement;
         class X509RevocationMode;
         class X509EnhancedKeyUsageExtension;
         class X509ChainStatus;
         class X509ExtensionEnumerator;
         class X509CertificateCollection;
         class X509CertificateEnumerator;
         class X509Certificate2Collection;
         class X509ChainElementCollection;
         class X509Store;
           class Oid;
           class AsnEncodedData;
           class OidCollection;
    static class X509SubjectKeyIdentifierHashAlgorithm // enum
    {
        static SIGNED_LONG_INTEGER Sha1;
        static SIGNED_LONG_INTEGER ShortSha1;
        static SIGNED_LONG_INTEGER CapiSha1;
    };

     class X509SubjectKeyIdentifierExtension 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION CopyFrom ( AsnEncodedData encodedData );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING SubjectKeyIdentifier[];
        Oid Oid;
    };

    static class X509NameType // enum
    {
        static SIGNED_LONG_INTEGER SimpleName;
        static SIGNED_LONG_INTEGER EmailName;
        static SIGNED_LONG_INTEGER UpnName;
        static SIGNED_LONG_INTEGER DnsName;
        static SIGNED_LONG_INTEGER DnsFromAlternativeName;
        static SIGNED_LONG_INTEGER UrlName;
    };

     class X509KeyUsageExtension 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION CopyFrom ( AsnEncodedData encodedData );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509KeyUsageFlags KeyUsages;
        Oid Oid;
    };

    static class X509ChainStatusFlags // enum
    {
        static SIGNED_LONG_INTEGER NoError;
        static SIGNED_LONG_INTEGER NotTimeValid;
        static SIGNED_LONG_INTEGER NotTimeNested;
        static SIGNED_LONG_INTEGER Revoked;
        static SIGNED_LONG_INTEGER NotSignatureValid;
        static SIGNED_LONG_INTEGER NotValidForUsage;
        static SIGNED_LONG_INTEGER UntrustedRoot;
        static SIGNED_LONG_INTEGER RevocationStatusUnknown;
        static SIGNED_LONG_INTEGER Cyclic;
        static SIGNED_LONG_INTEGER InvalidExtension;
        static SIGNED_LONG_INTEGER InvalidPolicyConstraints;
        static SIGNED_LONG_INTEGER InvalidBasicConstraints;
        static SIGNED_LONG_INTEGER InvalidNameConstraints;
        static SIGNED_LONG_INTEGER HasNotSupportedNameConstraint;
        static SIGNED_LONG_INTEGER HasNotDefinedNameConstraint;
        static SIGNED_LONG_INTEGER HasNotPermittedNameConstraint;
        static SIGNED_LONG_INTEGER HasExcludedNameConstraint;
        static SIGNED_LONG_INTEGER PartialChain;
        static SIGNED_LONG_INTEGER CtlNotTimeValid;
        static SIGNED_LONG_INTEGER CtlNotSignatureValid;
        static SIGNED_LONG_INTEGER CtlNotValidForUsage;
        static SIGNED_LONG_INTEGER OfflineRevocation;
        static SIGNED_LONG_INTEGER NoIssuanceChainPolicy;
    };

     class X509Certificate 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION GetCertHashString ();
        STRING_FUNCTION GetEffectiveDateString ();
        STRING_FUNCTION GetExpirationDateString ();
        STRING_FUNCTION GetFormat ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION GetIssuerName ();
        STRING_FUNCTION GetKeyAlgorithm ();
        STRING_FUNCTION GetKeyAlgorithmParametersString ();
        STRING_FUNCTION GetName ();
        STRING_FUNCTION GetPublicKeyString ();
        STRING_FUNCTION GetRawCertDataString ();
        STRING_FUNCTION GetSerialNumberString ();
        STRING_FUNCTION ToString ();
        FUNCTION Import ( STRING fileName );
        FUNCTION Reset ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Issuer[];
        STRING Subject[];
    };

     class X509Certificate2 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Import ( STRING fileName );
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        STRING_FUNCTION GetCertHashString ();
        STRING_FUNCTION GetEffectiveDateString ();
        STRING_FUNCTION GetExpirationDateString ();
        STRING_FUNCTION GetFormat ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION GetIssuerName ();
        STRING_FUNCTION GetKeyAlgorithm ();
        STRING_FUNCTION GetKeyAlgorithmParametersString ();
        STRING_FUNCTION GetName ();
        STRING_FUNCTION GetPublicKeyString ();
        STRING_FUNCTION GetRawCertDataString ();
        STRING_FUNCTION GetSerialNumberString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509ExtensionCollection Extensions;
        STRING FriendlyName[];
        X500DistinguishedName IssuerName;
        PublicKey PublicKey;
        STRING SerialNumber[];
        Oid SignatureAlgorithm;
        X500DistinguishedName SubjectName;
        STRING Thumbprint[];
        SIGNED_LONG_INTEGER Version;
        STRING Issuer[];
        STRING Subject[];
    };

     class X509Certificate2Enumerator 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509Certificate2 Current;
    };

    static class StoreName // enum
    {
        static SIGNED_LONG_INTEGER AddressBook;
        static SIGNED_LONG_INTEGER AuthRoot;
        static SIGNED_LONG_INTEGER CertificateAuthority;
        static SIGNED_LONG_INTEGER Disallowed;
        static SIGNED_LONG_INTEGER My;
        static SIGNED_LONG_INTEGER Root;
        static SIGNED_LONG_INTEGER TrustedPeople;
        static SIGNED_LONG_INTEGER TrustedPublisher;
    };

    static class X509KeyStorageFlags // enum
    {
        static SIGNED_LONG_INTEGER DefaultKeySet;
        static SIGNED_LONG_INTEGER UserKeySet;
        static SIGNED_LONG_INTEGER MachineKeySet;
        static SIGNED_LONG_INTEGER Exportable;
        static SIGNED_LONG_INTEGER UserProtected;
        static SIGNED_LONG_INTEGER PersistKeySet;
    };

     class X509ChainPolicy 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        OidCollection ApplicationPolicy;
        OidCollection CertificatePolicy;
        X509Certificate2Collection ExtraStore;
        X509RevocationFlag RevocationFlag;
        X509RevocationMode RevocationMode;
        X509VerificationFlags VerificationFlags;
    };

    static class X509RevocationFlag // enum
    {
        static SIGNED_LONG_INTEGER EndCertificateOnly;
        static SIGNED_LONG_INTEGER EntireChain;
        static SIGNED_LONG_INTEGER ExcludeRoot;
    };

     class X509ExtensionCollection 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION Add ( X509Extension extension );
        FUNCTION CopyTo ( X509Extension array[] , SIGNED_LONG_INTEGER index );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Count;
    };

     class X509ChainElementEnumerator 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509ChainElement Current;
    };

    static class X500DistinguishedNameFlags // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Reversed;
        static SIGNED_LONG_INTEGER UseSemicolons;
        static SIGNED_LONG_INTEGER DoNotUsePlusSign;
        static SIGNED_LONG_INTEGER DoNotUseQuotes;
        static SIGNED_LONG_INTEGER UseCommas;
        static SIGNED_LONG_INTEGER UseNewLines;
        static SIGNED_LONG_INTEGER UseUTF8Encoding;
        static SIGNED_LONG_INTEGER UseT61Encoding;
        static SIGNED_LONG_INTEGER ForceUTF8Encoding;
    };

    static class OpenFlags // enum
    {
        static SIGNED_LONG_INTEGER ReadOnly;
        static SIGNED_LONG_INTEGER ReadWrite;
        static SIGNED_LONG_INTEGER MaxAllowed;
        static SIGNED_LONG_INTEGER OpenExistingOnly;
        static SIGNED_LONG_INTEGER IncludeArchived;
    };

    static class X509FindType // enum
    {
        static SIGNED_LONG_INTEGER FindByThumbprint;
        static SIGNED_LONG_INTEGER FindBySubjectName;
        static SIGNED_LONG_INTEGER FindBySubjectDistinguishedName;
        static SIGNED_LONG_INTEGER FindByIssuerName;
        static SIGNED_LONG_INTEGER FindByIssuerDistinguishedName;
        static SIGNED_LONG_INTEGER FindBySerialNumber;
        static SIGNED_LONG_INTEGER FindByTimeValid;
        static SIGNED_LONG_INTEGER FindByTimeNotYetValid;
        static SIGNED_LONG_INTEGER FindByTimeExpired;
        static SIGNED_LONG_INTEGER FindByTemplateName;
        static SIGNED_LONG_INTEGER FindByApplicationPolicy;
        static SIGNED_LONG_INTEGER FindByCertificatePolicy;
        static SIGNED_LONG_INTEGER FindByExtension;
        static SIGNED_LONG_INTEGER FindByKeyUsage;
        static SIGNED_LONG_INTEGER FindBySubjectKeyIdentifier;
    };

    static class X509KeyUsageFlags // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER EncipherOnly;
        static SIGNED_LONG_INTEGER CrlSign;
        static SIGNED_LONG_INTEGER KeyCertSign;
        static SIGNED_LONG_INTEGER KeyAgreement;
        static SIGNED_LONG_INTEGER DataEncipherment;
        static SIGNED_LONG_INTEGER KeyEncipherment;
        static SIGNED_LONG_INTEGER NonRepudiation;
        static SIGNED_LONG_INTEGER DigitalSignature;
        static SIGNED_LONG_INTEGER DecipherOnly;
    };

     class X509Chain 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509ChainElementCollection ChainElements;
        X509ChainPolicy ChainPolicy;
        X509ChainStatus ChainStatus[];
    };

     class X509BasicConstraintsExtension 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION CopyFrom ( AsnEncodedData asnEncodedData );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER PathLengthConstraint;
        Oid Oid;
    };

    static class X509IncludeOption // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER ExcludeRoot;
        static SIGNED_LONG_INTEGER EndCertOnly;
        static SIGNED_LONG_INTEGER WholeChain;
    };

    static class X509ContentType // enum
    {
        static SIGNED_LONG_INTEGER Unknown;
        static SIGNED_LONG_INTEGER Cert;
        static SIGNED_LONG_INTEGER SerializedCert;
        static SIGNED_LONG_INTEGER Pfx;
        static SIGNED_LONG_INTEGER SerializedStore;
        static SIGNED_LONG_INTEGER Pkcs7;
        static SIGNED_LONG_INTEGER Authenticode;
    };

    static class StoreLocation // enum
    {
        static SIGNED_LONG_INTEGER CurrentUser;
        static SIGNED_LONG_INTEGER LocalMachine;
    };

    static class X509VerificationFlags // enum
    {
        static SIGNED_LONG_INTEGER NoFlag;
        static SIGNED_LONG_INTEGER IgnoreNotTimeValid;
        static SIGNED_LONG_INTEGER IgnoreCtlNotTimeValid;
        static SIGNED_LONG_INTEGER IgnoreNotTimeNested;
        static SIGNED_LONG_INTEGER IgnoreInvalidBasicConstraints;
        static SIGNED_LONG_INTEGER AllowUnknownCertificateAuthority;
        static SIGNED_LONG_INTEGER IgnoreWrongUsage;
        static SIGNED_LONG_INTEGER IgnoreInvalidName;
        static SIGNED_LONG_INTEGER IgnoreInvalidPolicy;
        static SIGNED_LONG_INTEGER IgnoreEndRevocationUnknown;
        static SIGNED_LONG_INTEGER IgnoreCtlSignerRevocationUnknown;
        static SIGNED_LONG_INTEGER IgnoreCertificateAuthorityRevocationUnknown;
        static SIGNED_LONG_INTEGER IgnoreRootRevocationUnknown;
        static SIGNED_LONG_INTEGER AllFlags;
    };

     class X509ChainElement 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509Certificate2 Certificate;
        X509ChainStatus ChainElementStatus[];
        STRING Information[];
    };

    static class X509RevocationMode // enum
    {
        static SIGNED_LONG_INTEGER NoCheck;
        static SIGNED_LONG_INTEGER Online;
        static SIGNED_LONG_INTEGER Offline;
    };

     class X509EnhancedKeyUsageExtension 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION CopyFrom ( AsnEncodedData asnEncodedData );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        OidCollection EnhancedKeyUsages;
        Oid Oid;
    };

     class X509ChainStatus 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509ChainStatusFlags Status;
        STRING StatusInformation[];
    };

     class X509ExtensionEnumerator 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509Extension Current;
    };

     class X509CertificateCollection 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION set_Item ( SIGNED_LONG_INTEGER index , X509Certificate value );
        SIGNED_LONG_INTEGER_FUNCTION Add ( X509Certificate value );
        FUNCTION AddRange ( X509Certificate value[] );
        FUNCTION CopyTo ( X509Certificate array[] , SIGNED_LONG_INTEGER index );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        SIGNED_LONG_INTEGER_FUNCTION IndexOf ( X509Certificate value );
        FUNCTION Insert ( SIGNED_LONG_INTEGER index , X509Certificate value );
        FUNCTION Remove ( X509Certificate value );
        FUNCTION Clear ();
        FUNCTION RemoveAt ( SIGNED_LONG_INTEGER index );
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Capacity;
        SIGNED_LONG_INTEGER Count;
    };

     class X509Certificate2Collection 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION set_Item ( SIGNED_LONG_INTEGER index , X509Certificate2 value );
        SIGNED_LONG_INTEGER_FUNCTION Add ( X509Certificate2 certificate );
        FUNCTION AddRange ( X509Certificate2 certificates[] );
        FUNCTION Import ( STRING fileName );
        FUNCTION Insert ( SIGNED_LONG_INTEGER index , X509Certificate2 certificate );
        FUNCTION Remove ( X509Certificate2 certificate );
        FUNCTION RemoveRange ( X509Certificate2 certificates[] );
        FUNCTION CopyTo ( X509Certificate array[] , SIGNED_LONG_INTEGER index );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        SIGNED_LONG_INTEGER_FUNCTION IndexOf ( X509Certificate value );
        FUNCTION Clear ();
        FUNCTION RemoveAt ( SIGNED_LONG_INTEGER index );
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Capacity;
        SIGNED_LONG_INTEGER Count;
    };

     class X509ChainElementCollection 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION CopyTo ( X509ChainElement array[] , SIGNED_LONG_INTEGER index );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Count;
    };

     class X509Store 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Add ( X509Certificate2 certificate );
        FUNCTION AddRange ( X509Certificate2Collection certificates );
        FUNCTION Close ();
        FUNCTION Open ( OpenFlags flags );
        FUNCTION Remove ( X509Certificate2 certificate );
        FUNCTION RemoveRange ( X509Certificate2Collection certificates );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509Certificate2Collection Certificates;
        StoreLocation Location;
        STRING Name[];
    };

namespace SSMono.Security.Cryptography;
        // class declarations
         class OidCollection;
         class AsnEncodedData;
         class AsnEncodedDataCollection;
         class OidEnumerator;
         class AsnEncodedDataEnumerator;
         class Oid;
     class OidCollection 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION Add ( Oid oid );
        FUNCTION CopyTo ( Oid array[] , SIGNED_LONG_INTEGER index );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Count;
    };

     class AsnEncodedDataCollection 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION Add ( AsnEncodedData asnEncodedData );
        FUNCTION CopyTo ( AsnEncodedData array[] , SIGNED_LONG_INTEGER index );
        FUNCTION Remove ( AsnEncodedData asnEncodedData );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Count;
    };

     class OidEnumerator 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        Oid Current;
    };

     class AsnEncodedDataEnumerator 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        AsnEncodedData Current;
    };

     class Oid 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING FriendlyName[];
        STRING Value[];
    };

namespace Mono.Security.X509;
        // class declarations
         class X509ChainStatusFlags;
         class X520;
         class AttributeTypeAndValue;
         class Name;
         class CommonName;
         class SerialNumber;
         class LocalityName;
         class StateOrProvinceName;
         class OrganizationName;
         class OrganizationalUnitName;
         class EmailAddress;
         class DomainComponent;
         class UserId;
         class Oid;
         class Title;
         class CountryName;
         class DnQualifier;
         class Surname;
         class GivenName;
         class Initial;
         class X509StoreManager;
         class X509Extension;
         class X509Stores;
         class Names;
         class X509Store;
         class X509Builder;
         class X509CertificateBuilder;
         class X509CertificateCollection;
         class X509CertificateEnumerator;
         class TrustAnchors;
         class X509ExtensionCollection;
         class X509Certificate;
         class X501;
         class PKCS5;
         class PKCS9;
         class PKCS12;
         class DeriveBytes;
         class Purpose;
         class X509Chain;
         class X509Crl;
         class X509CrlEntry;
    static class X509ChainStatusFlags // enum
    {
        static SIGNED_LONG_INTEGER NoError;
        static SIGNED_LONG_INTEGER NotTimeValid;
        static SIGNED_LONG_INTEGER NotTimeNested;
        static SIGNED_LONG_INTEGER NotSignatureValid;
        static SIGNED_LONG_INTEGER UntrustedRoot;
        static SIGNED_LONG_INTEGER InvalidBasicConstraints;
        static SIGNED_LONG_INTEGER PartialChain;
    };

     class X520 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class AttributeTypeAndValue 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class Name 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class CommonName 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class SerialNumber 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class LocalityName 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class StateOrProvinceName 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class OrganizationName 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class OrganizationalUnitName 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class EmailAddress 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class DomainComponent 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class UserId 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class Title 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class CountryName 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class DnQualifier 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class Surname 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class GivenName 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class Initial 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

     class X509StoreManager 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509Stores CurrentUser;
        X509Stores LocalMachine;
        X509CertificateCollection IntermediateCACertificates;
        X509CertificateCollection TrustedRootCertificates;
        X509CertificateCollection UntrustedCertificates;
    };

     class X509Stores 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509Store Personal;
        X509Store OtherPeople;
        X509Store IntermediateCA;
        X509Store TrustedRoot;
        X509Store Untrusted;
    };

     class Names 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING Personal[];
        static STRING OtherPeople[];
        static STRING IntermediateCA[];
        static STRING TrustedRoot[];
        static STRING Untrusted[];

        // class properties
    };

     class X509Store 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Clear ();
        FUNCTION Import ( X509Certificate certificate );
        FUNCTION Remove ( X509Certificate certificate );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509CertificateCollection Certificates;
        STRING Name[];
    };

     class X509Builder 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Hash[];
    };

     class X509CertificateBuilder 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING IssuerName[];
        STRING SubjectName[];
        X509ExtensionCollection Extensions;
        STRING Hash[];
    };

     class X509CertificateCollection 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION set_Item ( SIGNED_LONG_INTEGER index , X509Certificate value );
        SIGNED_LONG_INTEGER_FUNCTION Add ( X509Certificate value );
        FUNCTION AddRange ( X509Certificate value[] );
        FUNCTION CopyTo ( X509Certificate array[] , SIGNED_LONG_INTEGER index );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        SIGNED_LONG_INTEGER_FUNCTION IndexOf ( X509Certificate value );
        FUNCTION Insert ( SIGNED_LONG_INTEGER index , X509Certificate value );
        FUNCTION Remove ( X509Certificate value );
        FUNCTION Clear ();
        FUNCTION RemoveAt ( SIGNED_LONG_INTEGER index );
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Capacity;
        SIGNED_LONG_INTEGER Count;
    };

     class TrustAnchors 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509CertificateCollection Anchors;
    };

     class X509ExtensionCollection 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION Add ( X509Extension extension );
        FUNCTION AddRange ( X509Extension extension[] );
        FUNCTION CopyTo ( X509Extension extensions[] , SIGNED_LONG_INTEGER index );
        SIGNED_LONG_INTEGER_FUNCTION IndexOf ( X509Extension extension );
        FUNCTION Insert ( SIGNED_LONG_INTEGER index , X509Extension extension );
        FUNCTION Remove ( X509Extension extension );
        FUNCTION Clear ();
        FUNCTION RemoveAt ( SIGNED_LONG_INTEGER index );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Capacity;
        SIGNED_LONG_INTEGER Count;
    };

     class X501 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class PKCS5 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING pbeWithMD2AndDESCBC[];
        static STRING pbeWithMD5AndDESCBC[];
        static STRING pbeWithMD2AndRC2CBC[];
        static STRING pbeWithMD5AndRC2CBC[];
        static STRING pbeWithSHA1AndDESCBC[];
        static STRING pbeWithSHA1AndRC2CBC[];

        // class properties
    };

     class PKCS9 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING friendlyName[];
        static STRING localKeyId[];

        // class properties
    };

     class PKCS12 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION AddCertificate ( X509Certificate cert );
        FUNCTION RemoveCertificate ( X509Certificate cert );
        FUNCTION SaveToFile ( STRING filename );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING pbeWithSHAAnd128BitRC4[];
        static STRING pbeWithSHAAnd40BitRC4[];
        static STRING pbeWithSHAAnd3KeyTripleDESCBC[];
        static STRING pbeWithSHAAnd2KeyTripleDESCBC[];
        static STRING pbeWithSHAAnd128BitRC2CBC[];
        static STRING pbeWithSHAAnd40BitRC2CBC[];
        static STRING keyBag[];
        static STRING pkcs8ShroudedKeyBag[];
        static STRING certBag[];
        static STRING crlBag[];
        static STRING secretBag[];
        static STRING safeContentsBag[];
        static STRING x509Certificate[];
        static STRING sdsiCertificate[];
        static STRING x509Crl[];
        static SIGNED_LONG_INTEGER CryptoApiPasswordLimit;

        // class properties
        STRING Password[];
        SIGNED_LONG_INTEGER IterationCount;
        X509CertificateCollection Certificates;
        SIGNED_LONG_INTEGER MaximumPasswordLength;
    };

     class DeriveBytes 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING HashName[];
        SIGNED_LONG_INTEGER IterationCount;
    };

    static class Purpose // enum
    {
        static SIGNED_LONG_INTEGER Key;
        static SIGNED_LONG_INTEGER IV;
        static SIGNED_LONG_INTEGER MAC;
    };

     class X509Chain 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION LoadCertificate ( X509Certificate x509 );
        FUNCTION LoadCertificates ( X509CertificateCollection collection );
        FUNCTION Reset ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509CertificateCollection Chain;
        X509Certificate Root;
        X509ChainStatusFlags Status;
        X509CertificateCollection TrustAnchors;
    };

     class X509CrlEntry 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509ExtensionCollection Extensions;
    };

namespace Mono.Security.X509.Extensions;
        // class declarations
         class NetscapeCertTypeExtension;
         class CertTypes;
         class SubjectAltNameExtension;
         class KeyAttributesExtension;
         class SubjectKeyIdentifierExtension;
         class PrivateKeyUsagePeriodExtension;
         class KeyUsages;
         class KeyUsageExtension;
         class CRLDistributionPointsExtension;
         class DistributionPoint;
         class ReasonFlags;
         class CertificatePoliciesExtension;
         class BasicConstraintsExtension;
         class AuthorityKeyIdentifierExtension;
         class ExtendedKeyUsageExtension;
     class NetscapeCertTypeExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING Oid[];
    };

    static class CertTypes // enum
    {
        static SIGNED_LONG_INTEGER ObjectSigningCA;
        static SIGNED_LONG_INTEGER SmimeCA;
        static SIGNED_LONG_INTEGER SslCA;
        static SIGNED_LONG_INTEGER ObjectSigning;
        static SIGNED_LONG_INTEGER Smime;
        static SIGNED_LONG_INTEGER SslServer;
        static SIGNED_LONG_INTEGER SslClient;
    };

     class SubjectAltNameExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING RFC822[][];
        STRING DNSNames[][];
        STRING IPAddresses[][];
        STRING UniformResourceIdentifiers[][];
        STRING Oid[];
    };

     class KeyAttributesExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING Oid[];
    };

     class SubjectKeyIdentifierExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING Oid[];
    };

     class PrivateKeyUsagePeriodExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING Oid[];
    };

    static class KeyUsages // enum
    {
        static SIGNED_LONG_INTEGER none;
        static SIGNED_LONG_INTEGER encipherOnly;
        static SIGNED_LONG_INTEGER cRLSign;
        static SIGNED_LONG_INTEGER keyCertSign;
        static SIGNED_LONG_INTEGER keyAgreement;
        static SIGNED_LONG_INTEGER dataEncipherment;
        static SIGNED_LONG_INTEGER keyEncipherment;
        static SIGNED_LONG_INTEGER nonRepudiation;
        static SIGNED_LONG_INTEGER digitalSignature;
        static SIGNED_LONG_INTEGER decipherOnly;
    };

     class KeyUsageExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        KeyUsages KeyUsage;
        STRING Name[];
        STRING Oid[];
    };

     class CRLDistributionPointsExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING Oid[];
    };

    static class ReasonFlags // enum
    {
        static SIGNED_LONG_INTEGER Unused;
        static SIGNED_LONG_INTEGER KeyCompromise;
        static SIGNED_LONG_INTEGER CACompromise;
        static SIGNED_LONG_INTEGER AffiliationChanged;
        static SIGNED_LONG_INTEGER Superseded;
        static SIGNED_LONG_INTEGER CessationOfOperation;
        static SIGNED_LONG_INTEGER CertificateHold;
        static SIGNED_LONG_INTEGER PrivilegeWithdrawn;
        static SIGNED_LONG_INTEGER AACompromise;
    };

     class CertificatePoliciesExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING Oid[];
    };

     class BasicConstraintsExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static SIGNED_LONG_INTEGER NoPathLengthConstraint;

        // class properties
        STRING Name[];
        SIGNED_LONG_INTEGER PathLenConstraint;
        STRING Oid[];
    };

     class AuthorityKeyIdentifierExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING Oid[];
    };

     class ExtendedKeyUsageExtension 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        STRING Oid[];
    };

namespace Mono.Security.Authenticode;
        // class declarations
         class AuthenticodeBase;
         class AuthenticodeFormatter;
         class AuthenticodeDeformatter;
         class PrivateKey;
         class Authority;
         class SoftwarePublisherCertificate;
     class AuthenticodeBase 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING spcIndirectDataContext[];

        // class properties
    };

     class AuthenticodeFormatter 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING spcIndirectDataContext[];

        // class properties
        Authority Authority;
        X509CertificateCollection Certificates;
        STRING Hash[];
        STRING Description[];
    };

     class AuthenticodeDeformatter 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING spcIndirectDataContext[];

        // class properties
        STRING FileName[];
        SIGNED_LONG_INTEGER Reason;
        X509CertificateCollection Certificates;
        X509Certificate SigningCertificate;
    };

     class PrivateKey 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Save ( STRING filename );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER KeyType;
    };

    static class Authority // enum
    {
        static SIGNED_LONG_INTEGER Individual;
        static SIGNED_LONG_INTEGER Commercial;
        static SIGNED_LONG_INTEGER Maximum;
    };

     class SoftwarePublisherCertificate 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509CertificateCollection Certificates;
    };

namespace Crestron.SimplSharp.CrestronIO;
        // class declarations
         class FileEx;
    static class FileEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace Mono.Security;
        // class declarations
         class PKCS7;
         class Oid;
         class ContentInfo;
         class EncryptedData;
         class EnvelopedData;
         class RecipientInfo;
         class SignedData;
         class SignerInfo;
         class SortedSet;
     class PKCS7 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class Oid 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static STRING rsaEncryption[];
        static STRING data[];
        static STRING signedData[];
        static STRING envelopedData[];
        static STRING signedAndEnvelopedData[];
        static STRING digestedData[];
        static STRING encryptedData[];
        static STRING contentType[];
        static STRING messageDigest[];
        static STRING signingTime[];
        static STRING countersignature[];

        // class properties
    };

     class ContentInfo 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING ContentType[];
    };

     class EncryptedData 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        ContentInfo ContentInfo;
        ContentInfo EncryptionAlgorithm;
    };

     class EnvelopedData 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        ContentInfo ContentInfo;
        ContentInfo EncryptionAlgorithm;
    };

     class RecipientInfo 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Oid[];
        STRING Issuer[];
        SIGNED_LONG_INTEGER Version;
    };

     class SignedData 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        X509CertificateCollection Certificates;
        ContentInfo ContentInfo;
        STRING HashName[];
        SignerInfo SignerInfo;
    };

     class SignerInfo 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING IssuerName[];
        X509Certificate Certificate;
        STRING HashName[];
    };

     class SortedSet 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace SSMono.Security;
        // class declarations
         class SecurityException;
     class SecurityException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

