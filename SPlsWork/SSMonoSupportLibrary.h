namespace SSMono.Security.Permissions;
        // class declarations
         class EnvironmentPermissionAttribute;
         class SecurityAction;
         class SecurityAttribute;
         class CodeAccessSecurityAttribute;
         class SecurityPermissionFlag;
         class SecurityPermissionAttribute;
         class PermissionState;
    static class SecurityAction // enum
    {
        static SIGNED_LONG_INTEGER Demand;
        static SIGNED_LONG_INTEGER Assert;
        static SIGNED_LONG_INTEGER Deny;
        static SIGNED_LONG_INTEGER PermitOnly;
        static SIGNED_LONG_INTEGER LinkDemand;
        static SIGNED_LONG_INTEGER InheritanceDemand;
        static SIGNED_LONG_INTEGER RequestMinimum;
        static SIGNED_LONG_INTEGER RequestOptional;
        static SIGNED_LONG_INTEGER RequestRefuse;
    };

     class SecurityAttribute 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SecurityAction Action;
    };

     class CodeAccessSecurityAttribute 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SecurityAction Action;
    };

    static class SecurityPermissionFlag // enum
    {
        static SIGNED_LONG_INTEGER NoFlags;
        static SIGNED_LONG_INTEGER Assertion;
        static SIGNED_LONG_INTEGER UnmanagedCode;
        static SIGNED_LONG_INTEGER SkipVerification;
        static SIGNED_LONG_INTEGER Execution;
        static SIGNED_LONG_INTEGER ControlThread;
        static SIGNED_LONG_INTEGER ControlEvidence;
        static SIGNED_LONG_INTEGER ControlPolicy;
        static SIGNED_LONG_INTEGER SerializationFormatter;
        static SIGNED_LONG_INTEGER ControlDomainPolicy;
        static SIGNED_LONG_INTEGER ControlPrincipal;
        static SIGNED_LONG_INTEGER ControlAppDomain;
        static SIGNED_LONG_INTEGER RemotingConfiguration;
        static SIGNED_LONG_INTEGER Infrastructure;
        static SIGNED_LONG_INTEGER BindingRedirects;
        static SIGNED_LONG_INTEGER AllFlags;
    };

    static class PermissionState // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Unrestricted;
    };

namespace SSMono;
        // class declarations
         class MarshalByRefObject;
         class OperatingSystem;
         class TryParsers;
         class Environment;
         class SpecialFolder;
         class ThreadStaticAttribute;
         class MonoTODOAttribute;
         class MonoDocumentationNoteAttribute;
         class MonoExtensionAttribute;
         class MonoInternalNoteAttribute;
         class MonoLimitationAttribute;
         class MonoNotSupportedAttribute;
     class MarshalByRefObject 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class TryParsers 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class Environment 
    {
        // class delegates

        // class events

        // class functions
        static STRING_FUNCTION ExpandEnvironmentVariables ( STRING name );
        static STRING_FUNCTION GetEnvironmentVariable ( STRING variable );
        static STRING_FUNCTION GetFolderPath ( SpecialFolder folder );
        static FUNCTION SetEnvironmentVariable ( STRING variable , STRING value );
        static FUNCTION FailFast ( STRING message );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING CurrentDirectory[];
        STRING MachineName[];
        STRING NewLine[];
        OperatingSystem OSVersion;
        STRING StackTrace[];
        SIGNED_LONG_INTEGER TickCount;
    };

    static class SpecialFolder // enum
    {
        static SIGNED_LONG_INTEGER Programs;
        static SIGNED_LONG_INTEGER ApplicationData;
        static SIGNED_LONG_INTEGER CommonApplicationData;
    };

     class ThreadStaticAttribute 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class MonoTODOAttribute 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Comment[];
    };

namespace SSMono.ComponentModel;
        // class declarations
         class BrowsableAttribute;
         class DescriptionAttribute;
         class DefaultEventAttribute;
     class DescriptionAttribute 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static SSMono.ComponentModel.DescriptionAttribute Default;

        // class properties
        STRING Description[];
    };

namespace SSMono.Security;
        // class declarations
         class SecurityElement;
         class SecurityAttribute;
         class CodeAccessPermission;
         class SecurityCriticalScope;
         class SecurityCriticalAttribute;
     class CodeAccessPermission 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Demand ();
        FUNCTION FromXml ( SecurityElement e );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class SecurityCriticalScope // enum
    {
        static SIGNED_LONG_INTEGER Explicit;
        static SIGNED_LONG_INTEGER Everything;
    };

     class SecurityCriticalAttribute 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SecurityCriticalScope Scope;
    };

namespace System;
        // class declarations
         class EnvironmentVariableTarget;
    static class EnvironmentVariableTarget // enum
    {
        static SIGNED_LONG_INTEGER Process;
        static SIGNED_LONG_INTEGER User;
        static SIGNED_LONG_INTEGER Machine;
    };

namespace SSMono.Web.UI;
        // class declarations

