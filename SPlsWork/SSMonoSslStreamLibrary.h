namespace SSMono.Security.Principal;
        // class declarations
         class IdentityReferenceCollection;
         class WindowsIdentity;
         class IdentityNotMappedException;
         class WindowsAccountType;
         class WellKnownSidType;
         class PrincipalPolicy;
         class WindowsPrincipal;
         class UserToken;
         class IdentityReference;
         class NTAccount;
         class TokenAccessLevels;
         class TokenImpersonationLevel;
         class SecurityIdentifier;
         class WindowsBuiltInRole;
     class IdentityReferenceCollection 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION set_Item ( SIGNED_LONG_INTEGER index , IdentityReference value );
        FUNCTION Add ( IdentityReference identity );
        FUNCTION Clear ();
        FUNCTION CopyTo ( IdentityReference array[] , SIGNED_LONG_INTEGER offset );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER Count;
    };

     class IdentityNotMappedException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        IdentityReferenceCollection UnmappedIdentities;
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

    static class WindowsAccountType // enum
    {
        static SIGNED_LONG_INTEGER Normal;
        static SIGNED_LONG_INTEGER Guest;
        static SIGNED_LONG_INTEGER System;
        static SIGNED_LONG_INTEGER Anonymous;
    };

    static class WellKnownSidType // enum
    {
        static SIGNED_LONG_INTEGER NullSid;
        static SIGNED_LONG_INTEGER WorldSid;
        static SIGNED_LONG_INTEGER LocalSid;
        static SIGNED_LONG_INTEGER CreatorOwnerSid;
        static SIGNED_LONG_INTEGER CreatorGroupSid;
        static SIGNED_LONG_INTEGER CreatorOwnerServerSid;
        static SIGNED_LONG_INTEGER CreatorGroupServerSid;
        static SIGNED_LONG_INTEGER NTAuthoritySid;
        static SIGNED_LONG_INTEGER DialupSid;
        static SIGNED_LONG_INTEGER NetworkSid;
        static SIGNED_LONG_INTEGER BatchSid;
        static SIGNED_LONG_INTEGER InteractiveSid;
        static SIGNED_LONG_INTEGER ServiceSid;
        static SIGNED_LONG_INTEGER AnonymousSid;
        static SIGNED_LONG_INTEGER ProxySid;
        static SIGNED_LONG_INTEGER EnterpriseControllersSid;
        static SIGNED_LONG_INTEGER SelfSid;
        static SIGNED_LONG_INTEGER AuthenticatedUserSid;
        static SIGNED_LONG_INTEGER RestrictedCodeSid;
        static SIGNED_LONG_INTEGER TerminalServerSid;
        static SIGNED_LONG_INTEGER RemoteLogonIdSid;
        static SIGNED_LONG_INTEGER LogonIdsSid;
        static SIGNED_LONG_INTEGER LocalSystemSid;
        static SIGNED_LONG_INTEGER LocalServiceSid;
        static SIGNED_LONG_INTEGER NetworkServiceSid;
        static SIGNED_LONG_INTEGER BuiltinDomainSid;
        static SIGNED_LONG_INTEGER BuiltinAdministratorsSid;
        static SIGNED_LONG_INTEGER BuiltinUsersSid;
        static SIGNED_LONG_INTEGER BuiltinGuestsSid;
        static SIGNED_LONG_INTEGER BuiltinPowerUsersSid;
        static SIGNED_LONG_INTEGER BuiltinAccountOperatorsSid;
        static SIGNED_LONG_INTEGER BuiltinSystemOperatorsSid;
        static SIGNED_LONG_INTEGER BuiltinPrintOperatorsSid;
        static SIGNED_LONG_INTEGER BuiltinBackupOperatorsSid;
        static SIGNED_LONG_INTEGER BuiltinReplicatorSid;
        static SIGNED_LONG_INTEGER BuiltinPreWindows2000CompatibleAccessSid;
        static SIGNED_LONG_INTEGER BuiltinRemoteDesktopUsersSid;
        static SIGNED_LONG_INTEGER BuiltinNetworkConfigurationOperatorsSid;
        static SIGNED_LONG_INTEGER AccountAdministratorSid;
        static SIGNED_LONG_INTEGER AccountGuestSid;
        static SIGNED_LONG_INTEGER AccountKrbtgtSid;
        static SIGNED_LONG_INTEGER AccountDomainAdminsSid;
        static SIGNED_LONG_INTEGER AccountDomainUsersSid;
        static SIGNED_LONG_INTEGER AccountDomainGuestsSid;
        static SIGNED_LONG_INTEGER AccountComputersSid;
        static SIGNED_LONG_INTEGER AccountControllersSid;
        static SIGNED_LONG_INTEGER AccountCertAdminsSid;
        static SIGNED_LONG_INTEGER AccountSchemaAdminsSid;
        static SIGNED_LONG_INTEGER AccountEnterpriseAdminsSid;
        static SIGNED_LONG_INTEGER AccountPolicyAdminsSid;
        static SIGNED_LONG_INTEGER AccountRasAndIasServersSid;
        static SIGNED_LONG_INTEGER NtlmAuthenticationSid;
        static SIGNED_LONG_INTEGER DigestAuthenticationSid;
        static SIGNED_LONG_INTEGER SChannelAuthenticationSid;
        static SIGNED_LONG_INTEGER ThisOrganizationSid;
        static SIGNED_LONG_INTEGER OtherOrganizationSid;
        static SIGNED_LONG_INTEGER BuiltinIncomingForestTrustBuildersSid;
        static SIGNED_LONG_INTEGER BuiltinPerformanceMonitoringUsersSid;
        static SIGNED_LONG_INTEGER BuiltinPerformanceLoggingUsersSid;
        static SIGNED_LONG_INTEGER BuiltinAuthorizationAccessSid;
        static SIGNED_LONG_INTEGER WinBuiltinTerminalServerLicenseServersSid;
        static SIGNED_LONG_INTEGER MaxDefined;
    };

    static class PrincipalPolicy // enum
    {
        static SIGNED_LONG_INTEGER UnauthenticatedPrincipal;
        static SIGNED_LONG_INTEGER NoPrincipal;
        static SIGNED_LONG_INTEGER WindowsPrincipal;
    };

     class IdentityReference 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Value[];
    };

    static class TokenAccessLevels // enum
    {
        static SIGNED_LONG_INTEGER AssignPrimary;
        static SIGNED_LONG_INTEGER Duplicate;
        static SIGNED_LONG_INTEGER Impersonate;
        static SIGNED_LONG_INTEGER Query;
        static SIGNED_LONG_INTEGER QuerySource;
        static SIGNED_LONG_INTEGER AdjustPrivileges;
        static SIGNED_LONG_INTEGER AdjustGroups;
        static SIGNED_LONG_INTEGER AdjustDefault;
        static SIGNED_LONG_INTEGER AdjustSessionId;
        static SIGNED_LONG_INTEGER Read;
        static SIGNED_LONG_INTEGER Write;
        static SIGNED_LONG_INTEGER AllAccess;
        static SIGNED_LONG_INTEGER MaximumAllowed;
    };

    static class TokenImpersonationLevel // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Anonymous;
        static SIGNED_LONG_INTEGER Identification;
        static SIGNED_LONG_INTEGER Impersonation;
        static SIGNED_LONG_INTEGER Delegation;
    };

    static class WindowsBuiltInRole // enum
    {
        static SIGNED_LONG_INTEGER Administrator;
        static SIGNED_LONG_INTEGER User;
        static SIGNED_LONG_INTEGER Guest;
        static SIGNED_LONG_INTEGER PowerUser;
        static SIGNED_LONG_INTEGER AccountOperator;
        static SIGNED_LONG_INTEGER SystemOperator;
        static SIGNED_LONG_INTEGER PrintOperator;
        static SIGNED_LONG_INTEGER BackupOperator;
        static SIGNED_LONG_INTEGER Replicator;
    };

namespace SSMono.Security;
        // class declarations
         class SecurityException;
         class SecurityZone;
         class PermissionSet;
     class SecurityException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

    static class SecurityZone // enum
    {
        static SIGNED_LONG_INTEGER MyComputer;
        static SIGNED_LONG_INTEGER Intranet;
        static SIGNED_LONG_INTEGER Trusted;
        static SIGNED_LONG_INTEGER Internet;
        static SIGNED_LONG_INTEGER Untrusted;
        static SIGNED_LONG_INTEGER NoZone;
    };

     class PermissionSet 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace Mono.Security.Protocol.Tls;
        // class declarations
         class ExchangeAlgorithmType;
         class SecurityCompressionType;
         class SslStreamBase;
         class SslServerStream;
         class ValidationResult;
         class HashAlgorithmType;
         class SslClientStream;
         class CipherAlgorithmType;
         class SecurityProtocolType;
    static class ExchangeAlgorithmType // enum
    {
        static SIGNED_LONG_INTEGER DiffieHellman;
        static SIGNED_LONG_INTEGER Fortezza;
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER RsaKeyX;
        static SIGNED_LONG_INTEGER RsaSign;
    };

    static class SecurityCompressionType // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Zlib;
    };

     class SslStreamBase 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION Flush ();
        FUNCTION Dispose ();
        SIGNED_LONG_INTEGER_FUNCTION ReadByte ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        CipherAlgorithmType CipherAlgorithm;
        SIGNED_LONG_INTEGER CipherStrength;
        HashAlgorithmType HashAlgorithm;
        SIGNED_LONG_INTEGER HashStrength;
        SIGNED_LONG_INTEGER KeyExchangeStrength;
        ExchangeAlgorithmType KeyExchangeAlgorithm;
        SecurityProtocolType SecurityProtocol;
        SIGNED_LONG_INTEGER ReadTimeout;
        SIGNED_LONG_INTEGER WriteTimeout;
    };

    static class HashAlgorithmType // enum
    {
        static SIGNED_LONG_INTEGER Md5;
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Sha1;
    };

    static class CipherAlgorithmType // enum
    {
        static SIGNED_LONG_INTEGER Des;
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Rc2;
        static SIGNED_LONG_INTEGER Rc4;
        static SIGNED_LONG_INTEGER Rijndael;
        static SIGNED_LONG_INTEGER SkipJack;
        static SIGNED_LONG_INTEGER TripleDes;
    };

    static class SecurityProtocolType // enum
    {
        static SIGNED_LONG_INTEGER Ssl2;
        static SIGNED_LONG_INTEGER Ssl3;
        static SIGNED_LONG_INTEGER Tls;
        static SIGNED_LONG_INTEGER Default;
    };

namespace SSMono.Net.Security;
        // class declarations
         class AuthenticatedStream;
         class NegotiateStream;
         class SslStream;
         class ProtectionLevel;
         class SslPolicyErrors;
           class SslProtocols;
     class AuthenticatedStream 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Close ();
        FUNCTION Dispose ();
        FUNCTION Flush ();
        SIGNED_LONG_INTEGER_FUNCTION ReadByte ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        SIGNED_LONG_INTEGER ReadTimeout;
        SIGNED_LONG_INTEGER WriteTimeout;
    };

    static class ProtectionLevel // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Sign;
        static SIGNED_LONG_INTEGER EncryptAndSign;
    };

    static class SslPolicyErrors // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER RemoteCertificateNotAvailable;
        static SIGNED_LONG_INTEGER RemoteCertificateNameMismatch;
        static SIGNED_LONG_INTEGER RemoteCertificateChainErrors;
    };

namespace SSMono.Security.Authentication;
        // class declarations
         class HashAlgorithmType;
         class CipherAlgorithmType;
         class ExchangeAlgorithmType;
         class SslProtocols;
         class AuthenticationException;
         class InvalidCredentialException;
    static class HashAlgorithmType // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Md5;
        static SIGNED_LONG_INTEGER Sha1;
    };

    static class CipherAlgorithmType // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Null;
        static SIGNED_LONG_INTEGER Des;
        static SIGNED_LONG_INTEGER Rc2;
        static SIGNED_LONG_INTEGER TripleDes;
        static SIGNED_LONG_INTEGER Aes128;
        static SIGNED_LONG_INTEGER Aes192;
        static SIGNED_LONG_INTEGER Aes256;
        static SIGNED_LONG_INTEGER Aes;
        static SIGNED_LONG_INTEGER Rc4;
    };

    static class ExchangeAlgorithmType // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER RsaSign;
        static SIGNED_LONG_INTEGER RsaKeyX;
        static SIGNED_LONG_INTEGER DiffieHellman;
    };

    static class SslProtocols // enum
    {
        static SIGNED_LONG_INTEGER None;
        static SIGNED_LONG_INTEGER Ssl2;
        static SIGNED_LONG_INTEGER Ssl3;
        static SIGNED_LONG_INTEGER Tls;
        static SIGNED_LONG_INTEGER Default;
    };

     class AuthenticationException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

     class InvalidCredentialException 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Message[];
        STRING StackTrace[];
        STRING HelpLink[];
        STRING Source[];
    };

