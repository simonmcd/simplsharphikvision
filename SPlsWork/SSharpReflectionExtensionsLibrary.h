namespace Crestron.SimplSharp.Reflection.Emit;
        // class declarations
         class OpCode;
         class OpCodeType;
         class OperandType;
         class OpCodes;
         class FlowControl;
         class StackBehaviour;
     class OpCode 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        INTEGER __class_id__;

        // class properties
        STRING Name[];
        SIGNED_LONG_INTEGER Size;
        OpCodeType OpCodeType;
        OperandType OperandType;
        FlowControl FlowControl;
        StackBehaviour StackBehaviourPop;
        StackBehaviour StackBehaviourPush;
        SIGNED_INTEGER Value;
    };

    static class OpCodeType // enum
    {
        static SIGNED_LONG_INTEGER Annotation;
        static SIGNED_LONG_INTEGER Macro;
        static SIGNED_LONG_INTEGER Nternal;
        static SIGNED_LONG_INTEGER Objmodel;
        static SIGNED_LONG_INTEGER Prefix;
        static SIGNED_LONG_INTEGER Primitive;
    };

    static class OperandType // enum
    {
        static SIGNED_LONG_INTEGER InlineBrTarget;
        static SIGNED_LONG_INTEGER InlineField;
        static SIGNED_LONG_INTEGER InlineI;
        static SIGNED_LONG_INTEGER InlineI8;
        static SIGNED_LONG_INTEGER InlineMethod;
        static SIGNED_LONG_INTEGER InlineNone;
        static SIGNED_LONG_INTEGER InlinePhi;
        static SIGNED_LONG_INTEGER InlineR;
        static SIGNED_LONG_INTEGER InlineSig;
        static SIGNED_LONG_INTEGER InlineString;
        static SIGNED_LONG_INTEGER InlineSwitch;
        static SIGNED_LONG_INTEGER InlineTok;
        static SIGNED_LONG_INTEGER InlineType;
        static SIGNED_LONG_INTEGER InlineVar;
        static SIGNED_LONG_INTEGER ShortInlineBrTarget;
        static SIGNED_LONG_INTEGER ShortInlineI;
        static SIGNED_LONG_INTEGER ShortInlineR;
        static SIGNED_LONG_INTEGER ShortInlineVar;
    };

     class OpCodes 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        static Crestron.SimplSharp.Reflection.Emit.OpCode Nop;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Break;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldarg_0;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldarg_1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldarg_2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldarg_3;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldloc_0;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldloc_1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldloc_2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldloc_3;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stloc_0;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stloc_1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stloc_2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stloc_3;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldarg_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldarga_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Starg_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldloc_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldloca_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stloc_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldnull;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_M1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_0;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_3;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_5;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_6;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_7;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_I8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_R4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldc_R8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Dup;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Pop;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Jmp;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Call;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Calli;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ret;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Br_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Brfalse_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Brtrue_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Beq_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bge_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bgt_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ble_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Blt_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bne_Un_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bge_Un_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bgt_Un_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ble_Un_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Blt_Un_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Br;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Brfalse;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Brtrue;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Beq;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bge;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bgt;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ble;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Blt;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bne_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bge_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Bgt_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ble_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Blt_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Switch;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_I1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_U1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_I2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_U2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_I4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_U4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_I8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_I;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_R4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_R8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldind_Ref;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stind_Ref;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stind_I1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stind_I2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stind_I4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stind_I8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stind_R4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stind_R8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Add;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Sub;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Mul;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Div;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Div_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Rem;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Rem_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode And;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Or;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Xor;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Shl;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Shr;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Shr_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Neg;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Not;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_I1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_I2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_I4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_I8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_R4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_R8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_U4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_U8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Callvirt;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Cpobj;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldobj;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldstr;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Newobj;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Castclass;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Isinst;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_R_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Unbox;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Throw;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldfld;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldflda;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stfld;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldsfld;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldsflda;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stsfld;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stobj;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I1_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I2_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I4_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I8_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U1_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U2_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U4_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U8_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Box;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Newarr;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldlen;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelema;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_I1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_U1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_I2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_U2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_I4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_U4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_I8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_I;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_R4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_R8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem_Ref;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stelem_I;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stelem_I1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stelem_I2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stelem_I4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stelem_I8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stelem_R4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stelem_R8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stelem_Ref;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldelem;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stelem;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Unbox_Any;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U8;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Refanyval;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ckfinite;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Mkrefany;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldtoken;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_U2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_U1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_I;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_I;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_Ovf_U;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Add_Ovf;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Add_Ovf_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Mul_Ovf;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Mul_Ovf_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Sub_Ovf;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Sub_Ovf_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Endfinally;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Leave;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Leave_S;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stind_I;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Conv_U;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Prefix7;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Prefix6;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Prefix5;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Prefix4;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Prefix3;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Prefix2;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Prefix1;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Prefixref;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Arglist;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ceq;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Cgt;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Cgt_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Clt;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Clt_Un;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldftn;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldvirtftn;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldarg;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldarga;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Starg;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldloc;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Ldloca;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Stloc;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Localloc;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Endfilter;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Unaligned;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Volatile;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Tailcall;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Initobj;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Constrained;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Cpblk;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Initblk;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Rethrow;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Sizeof;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Refanytype;
        static Crestron.SimplSharp.Reflection.Emit.OpCode Readonly;

        // class properties
    };

    static class FlowControl // enum
    {
        static SIGNED_LONG_INTEGER Branch;
        static SIGNED_LONG_INTEGER Break;
        static SIGNED_LONG_INTEGER Call;
        static SIGNED_LONG_INTEGER Cond_Branch;
        static SIGNED_LONG_INTEGER Meta;
        static SIGNED_LONG_INTEGER Next;
        static SIGNED_LONG_INTEGER Phi;
        static SIGNED_LONG_INTEGER Return;
        static SIGNED_LONG_INTEGER Throw;
    };

    static class StackBehaviour // enum
    {
        static SIGNED_LONG_INTEGER Pop0;
        static SIGNED_LONG_INTEGER Pop1;
        static SIGNED_LONG_INTEGER Pop1_pop1;
        static SIGNED_LONG_INTEGER Popi;
        static SIGNED_LONG_INTEGER Popi_pop1;
        static SIGNED_LONG_INTEGER Popi_popi;
        static SIGNED_LONG_INTEGER Popi_popi8;
        static SIGNED_LONG_INTEGER Popi_popi_popi;
        static SIGNED_LONG_INTEGER Popi_popr4;
        static SIGNED_LONG_INTEGER Popi_popr8;
        static SIGNED_LONG_INTEGER Popref;
        static SIGNED_LONG_INTEGER Popref_pop1;
        static SIGNED_LONG_INTEGER Popref_popi;
        static SIGNED_LONG_INTEGER Popref_popi_popi;
        static SIGNED_LONG_INTEGER Popref_popi_popi8;
        static SIGNED_LONG_INTEGER Popref_popi_popr4;
        static SIGNED_LONG_INTEGER Popref_popi_popr8;
        static SIGNED_LONG_INTEGER Popref_popi_popref;
        static SIGNED_LONG_INTEGER Push0;
        static SIGNED_LONG_INTEGER Push1;
        static SIGNED_LONG_INTEGER Push1_push1;
        static SIGNED_LONG_INTEGER Pushi;
        static SIGNED_LONG_INTEGER Pushi8;
        static SIGNED_LONG_INTEGER Pushr4;
        static SIGNED_LONG_INTEGER Pushr8;
        static SIGNED_LONG_INTEGER Pushref;
        static SIGNED_LONG_INTEGER Varpop;
        static SIGNED_LONG_INTEGER Varpush;
        static SIGNED_LONG_INTEGER Popref_popi_pop1;
    };

namespace Crestron.SimplSharp.Reflection;
        // class declarations
         class MethodInfoExtensions;
         class CTypeExtensions;
         class PropertyInfoExtensions;
         class MethodBaseEx;
         class TypeExtensions;
         class AssemblyEx;
         class ActivatorEx;
         class CDelegateEx;
    static class MethodInfoExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class CTypeExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class PropertyInfoExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class MethodBaseEx 
    {
        // class delegates

        // class events

        // class functions
        static STRING_FUNCTION GetCurrentMethodNameWithParameters ();
        static STRING_FUNCTION GetStackMethodNameWithParameters ( SIGNED_LONG_INTEGER level );
        static STRING_FUNCTION GetCurrentMethodName ();
        static STRING_FUNCTION GetStackMethodName ( SIGNED_LONG_INTEGER level );
        static STRING_FUNCTION GetCallingMethodNameWithParameters ();
        static STRING_FUNCTION GetCallingMethodName ();
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class TypeExtensions 
    {
        // class delegates

        // class events

        // class functions
        static FUNCTION SplitFullyQualifiedTypeName ( STRING fullyQualifiedTypeName , BYREF STRING typeName , BYREF STRING assemblyName );
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class AssemblyEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class ActivatorEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

     class CDelegateEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace Crestron.SimplSharp.CrestronIO;
        // class declarations
         class AsyncResult;
         class AsyncCallbackExtensions;
     class AsyncResult 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class AsyncCallbackExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

namespace Crestron.SimplSharp;
        // class declarations
         class DelegateExtensions;
         class DelegateEx;
    static class DelegateExtensions 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

    static class DelegateEx 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION ToString ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();

        // class variables
        INTEGER __class_id__;

        // class properties
    };

