﻿using System;
using System.Text;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes
using SimplSharpHTTPLibrary;
using System.Collections.Generic;
using Newtonsoft.Json;
using Crestron.SimplSharp.Net.Http;
using Crestron.SimplSharp.CrestronXml;

namespace HikvisionLibrary
{
    public class HikvisionClient
    {
        // class instantation
        public SimplSharpWebClient MySimplSharpWebClient;
        //public State MySensorState;

        // Public delegates

        public OutputSensorStateDel OutputSensorState { get; set; }
        public delegate void OutputSensorStateDel(  ushort enabled,
                                                    SimplSharpString detection,
                                                    ushort lowLight,
                                                    SimplSharpString walkTest,
                                                    ushort tempValue,
                                                    SimplSharpString tempUnit,
                                                    SimplSharpString digitalOutput1,
                                                    SimplSharpString digitalOutput2,
                                                    SimplSharpString tamper);

        // class variables
        private int port;
        private string address, username, password;

        /// <summary>
        /// SIMPL+ can only execute the default constructor. If you have variables that require initialization, please
        /// use an Initialize method
        /// </summary>
        public HikvisionClient()
        {
        }

        public void InitClient(String initAddress, int initPort, String initUsername, String initPassword)
        {
            MySimplSharpWebClient = new SimplSharpWebClient();
            address = initAddress;
            port = initPort;
            username = initUsername;
            password = initPassword;
            CrestronConsole.PrintLine("Hikvision Init address={0} port={1} username={2} password={3}\n\r", address, port, username, password);
        }

        public void GetInfo()
        {
            SendGet("/ISAPI/DisplayDev/Video/streaming/channels");
        }

        private void SendGet(String urlCmdPath)
        {
            String returnedFromGet = "";
            CrestronConsole.Print("SendGet Called");

            returnedFromGet = MySimplSharpWebClient.SendGet(address, port, urlCmdPath, username, password);
            CrestronConsole.Print("SendGet String Returned: " + returnedFromGet + "\r\n");

            try
            {
                CrestronConsole.PrintLine("Extractor Starting...\n\r");    //Generate error
                XmlReader MyXmlReader = new XmlReader(returnedFromGet);

                switch (MyXmlReader.Name)
                {
                    case ("name"):
                        {
                            CrestronConsole.PrintLine("Cam Name: {0}\n\r", MyXmlReader.ReadString());    //Generate error
                            break;
                        }
                }

                CrestronConsole.PrintLine("Extractor Finished...\n\r");    //Generate error 
            }
            catch (Exception e)
            {
                CrestronConsole.Print("XML Exception Thrown: {0}\r\n", e);
            }
        }
    }
}
