﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;

namespace Project1
{
    public class HttpTest
    {

        /// <summary>
        /// SIMPL+ can only execute the default constructor. If you have variables that require initialization, please
        /// use an Initialize method
        /// </summary>
        public HttpTest()
        {
             
        }

        public static void Main(string[] args)
        {
            //Send Post request
            try
            {
                string url = "http://172.20.200.102:80/ISAPI/DisplayDev/VideoWall/1/windows";
                string body = @"<WallWindow xmlns=""http://www.isapi.org/ver20/XMLSchema"" version=""2.0""><id/><wndOperateMode>uniformCoordinate</wndOperateMode><Rect><Coordinate><x>0</x><y>0</y></Coordinate><width>1920</width><height>1920</height></Rect><layerIdx/><windowMode>1</windowMode><SubWindowList><SubWindow><id>1</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>1025</streamingChannelID></SubWindowParam></SubWindow></SubWindowList></WallWindow>";
                string username = "admin";
                string password = "mnbvcxz3467";

                Uri myUri = new Uri(url);
                WebRequest myHttpWebRequest = WebRequest.Create(myUri);

                NetworkCredential myNetworkCredential = new NetworkCredential(username, password);
                CredentialCache myCredentialCache = new CredentialCache();
                myCredentialCache.Add(myUri, "Digest", myNetworkCredential);

                myHttpWebRequest.PreAuthenticate = true;
                myHttpWebRequest.Credentials = myCredentialCache;
                myHttpWebRequest.Method = "POST";

                if (myHttpWebRequest.Method == "POST")
                {
                    //Console.write("POST Request 1a\r\n");
                    byte[] bytes = Encoding.ASCII.GetBytes(body);
                    myHttpWebRequest.ContentLength = bytes.Length;

                    try
                    {
                        Stream requestStream = myHttpWebRequest.GetRequestStream();
                        // Console.write("POST Request 1b\r\n");
                        requestStream.Write(bytes, 0, bytes.Length);
                        // Console.write("POST Request 1c\r\n");
                        requestStream.Flush();
                        requestStream.Close();
                        // Console.write("POST Request 2\r\n");
                        WebResponse myWebResponse = myHttpWebRequest.GetResponse();
                        // Console.write("POST Request 3\r\n");
                        Stream responseStream = myWebResponse.GetResponseStream();
                        // Console.write("POST Request 4\r\n");
                        StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                        //Console.write("POST Request 5\r\n");
                        string pageContent = myStreamReader.ReadToEnd();
                        //Console.write("POST Request 6\r\n");
                        responseStream.Close();
                        myWebResponse.Close();
                    }
                    catch (Exception e)
                    {
                        Console.Write("WebClient GetRequestStream Exception Thrown: {0}\r\n", e);
                    }
                }

                //Print the data that was returned from the Post request
                //CrestronConsole.Print("Data that was returned from Post request: " + pageContent + "\r\n");
                //return pageContent;                
            }
            catch (Exception e)
            {
                Console.Write("WebClient Post Request Exception Thrown: {0}\r\n", e);
                //return null;
            }

            Console.ReadLine();
        }      
    }
}
