﻿using System;
using System.Text;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes
using SimplSharpHTTPLibrary;
using System.Collections.Generic;
using Crestron.SimplSharp.Net.Http;
using Crestron.SimplSharp.CrestronIO;

namespace HikvisionLibrary
{
    public class HikvisionPtz
    {
        // class instantation
        public SimplSharpHTTPClient MySimplSharpHTTPClient;

        // class variables
        private int port;
        private string address, username, password, baseURL;

        /// <summary>
        /// SIMPL+ can only execute the default constructor. If you have variables that require initialization, please
        /// use an Initialize method
        /// </summary>
        public HikvisionPtz()
        {
        }

        public void Init(String initAddress, int initPort, String initUsername, String initPassword)
        {
            MySimplSharpHTTPClient = new SimplSharpHTTPClient();
            
            address = initAddress;
            port = initPort;
            username = initUsername;
            password = initPassword;
            baseURL = String.Format(@"http://{0}:{1}", address, port);
            CrestronConsole.PrintLine("Hikvision PTZ Init address={0} port={1} username={2} password={3}\n\r", address, port, username, password);
        }

        public void GoToPreset(int channel, int preset)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=GOTO_PRESET&presetNo={2}&mode=start", baseURL, channel, preset);
            SendPut(urlPath);
        }

        public void SetPreset(int channel, int preset)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=SET_PRESET&presetNo={2}&mode=start", baseURL, channel, preset);
            SendPut(urlPath);
        }

        public void PanLeftStart(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=PAN_LEFT&mode=start&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);                 
        }

        public void PanLeftStop(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=PAN_LEFT&mode=stop&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void PanRightStart(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=PAN_RIGHT&mode=start&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void PanRightStop(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=PAN_RIGHT&mode=stop&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void TiltUpStart(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=TILT_UP&mode=start&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void TiltUpStop(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=TILT_UP&mode=stop&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void TiltDownStart(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=TILT_DOWN&mode=start&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void TiltDownStop(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=TILT_DOWN&mode=stop&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void UpLeftStart(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=UP_LEFT&mode=start&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void UpLeftStop(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=UP_LEFT&mode=stop&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void UpRightStart(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=UP_RIGHT&mode=start&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void UpRightStop(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=UP_RIGHT&mode=stop&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void DownLeftStart(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=DOWN_LEFT&mode=start&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void DownLeftStop(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=DOWN_LEFT&mode=stop&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void DownRightStart(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=DOWN_RIGHT&mode=start&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void DownRightStop(int channel, int speed)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=DOWN_RIGHT&mode=stop&speed={2}", baseURL, channel, speed);
            SendPut(urlPath);
        }

        public void ZoomInStart(int channel)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=ZOOM_OUT&mode=start", baseURL, channel);
            SendPut(urlPath);
        }

        public void ZoomInStop(int channel)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=ZOOM_OUT&mode=stop", baseURL, channel);
            SendPut(urlPath);
        }

        public void ZoomOutStart(int channel)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=ZOOM_IN&mode=start", baseURL, channel);
            SendPut(urlPath);
        }

        public void ZoomOutStop(int channel)
        {
            String urlPath = String.Format(@"{0}/PTZ/channels/{1}/PTZControl?command=ZOOM_IN&mode=stop", baseURL, channel);
            SendPut(urlPath);
        }

        public void SendCmd(string cmdStr)
        {
            String urlPath = String.Format(@"{0}{1}", baseURL, cmdStr);
            SendPut(urlPath);
        }

        private void SendPut(String url)
        {
            CrestronConsole.PrintLine("Hikvision PTZ SendPut url={0}\n\r", url);
            MySimplSharpHTTPClient.SendPut(url, username, password);         
        }
    }
}