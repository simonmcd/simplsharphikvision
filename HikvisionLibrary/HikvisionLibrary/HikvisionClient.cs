﻿using System;
using System.Text;
using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes
using SimplSharpHTTPLibrary;
using System.Collections.Generic;
using Newtonsoft.Json;
using Crestron.SimplSharp.Net.Http;
using Crestron.SimplSharp.CrestronXml;
using Crestron.SimplSharp.CrestronIO;
using SSMono.Net;

namespace HikvisionLibrary
{
    public class HikvisionClient
    {
        // class instantation
        public SimplSharpWebClient MySimplSharpWebClient;
        public HttpClient MyHttpClient;

        // Public delegates
        public OutputCameraDel OutputCamera { get; set; }
        public delegate void OutputCameraDel(SimplSharpString camName,
                                             ushort id,
                                             SimplSharpString group,
                                             ushort index);

        public OutputSceneDel OutputScene { get; set; }
        public delegate void OutputSceneDel(SimplSharpString sceneName,
                                             ushort id,
                                             ushort index);

        // class variables
        private int port;
        private string address, username, password, baseURL;
        private Camera[] MyCameraList;
        private Scene[] MySceneList;

        /// <summary>
        /// SIMPL+ can only execute the default constructor. If you have variables that require initialization, please
        /// use an Initialize method
        /// </summary>
        public HikvisionClient()
        {
        }

        public void Init(String initAddress, int initPort, String initUsername, String initPassword, ushort maxCamera, ushort maxScene)
        {
            MySimplSharpWebClient = new SimplSharpWebClient();
            MyHttpClient = new HttpClient();
            
            MyCameraList = new Camera[maxCamera-1];
            for (int i = 0; i < maxCamera-1; i++)
                MyCameraList[i] = new Camera();

            MySceneList = new Scene[maxScene-1];
            for (int i = 0; i < maxScene-1; i++)
                MySceneList[i] = new Scene();

            address = initAddress;
            port = initPort;
            username = initUsername;
            password = initPassword;
            baseURL = String.Format(@"http://{0}:{1}", address, port);
            CrestronConsole.PrintLine("Hikvision Init address={0} port={1} username={2} password={3}\n\r", address, port, username, password);
        }

        public void GetInfo()
        {
            SendGet("/ISAPI/DisplayDev/Video/streaming/channels");
            //SendGet("/ISAPI/DisplayDev/VideoWall/1/scene");
        }

        public void SetSceneFull(int window, int camera)
        {
            string body = @"<WallWindow xmlns=""http://www.isapi.org/ver20/XMLSchema"" version=""2.0""><id>1</id><windowMode>1</windowMode><SubWindowList><SubWindow><id>1</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera].id + "</streamingChannelID></SubWindowParam></SubWindow></SubWindowList></WallWindow>";
            CrestronConsole.Print("SetSceneFull Window={0} Cam{1}\r\n", window, camera);

            SendPut(baseURL + "/ISAPI/DisplayDev/VideoWall/1/window/" + window, body);
        }

        public void SetScene2x2(int window, int camera1, int camera2, int camera3, int camera4)
        {
            string body = @"<WallWindow xmlns=""http://www.isapi.org/ver20/XMLSchema"" version=""2.0""><id>1</id><windowMode>4</windowMode><SubWindowList><SubWindow><id>1</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera1].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>2</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera2].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>3</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera3].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>4</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera4].id + "</streamingChannelID></SubWindowParam></SubWindow></SubWindowList></WallWindow>";
            CrestronConsole.Print("SetScene2x2 Window={0} Cam{1} {2} {3} {4}\r\n", window, camera1, camera2, camera3, camera4);
            CrestronConsole.Print("Body = {0}", body);

            SendPut(baseURL + "/ISAPI/DisplayDev/VideoWall/1/window/" + window, body);
        }

        public void SetScene3x3(int window, int camera1, int camera2, int camera3, int camera4, int camera5, int camera6, int camera7, int camera8, int camera9)
        {
            string body = @"<WallWindow xmlns=""http://www.isapi.org/ver20/XMLSchema"" version=""2.0""><id>1</id><windowMode>9</windowMode><SubWindowList><SubWindow><id>1</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera1].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>2</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera2].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>3</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera3].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>4</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera4].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>5</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera5].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>6</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera6].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>7</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera7].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>8</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera8].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>9</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera9].id + "</streamingChannelID></SubWindowParam></SubWindow></SubWindowList></WallWindow>";
            CrestronConsole.Print("SetScene3x3 Window={0} Cam{1} {2} {3} {4} {5} {6} {7} {8} {9}\r\n", window, camera1, camera2, camera3, camera4, camera5, camera6, camera7, camera8, camera9);
            CrestronConsole.Print("Body = {0}", body);

            SendPut(baseURL + "/ISAPI/DisplayDev/VideoWall/1/window/" + window, body);
            SendPut(baseURL + "/ISAPI/DisplayDev/VideoWall/1/window/" + window, body);      // send twice as sometimes not all cameras load on the screen, don't know why!
        }

        public void SetScene1Plus5(int window, int camera1, int camera2, int camera3, int camera4, int camera5, int camera6)
        {
            string body = @"<WallWindow xmlns=""http://www.isapi.org/ver20/XMLSchema"" version=""2.0""><id>1</id><windowMode>6</windowMode><SubWindowList><SubWindow><id>1</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera1].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>2</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera2].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>3</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera3].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>4</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera4].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>5</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera5].id + "</streamingChannelID></SubWindowParam></SubWindow><SubWindow><id>6</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera6].id + "</streamingChannelID></SubWindowParam></SubWindow></SubWindowList></WallWindow>";
            CrestronConsole.Print("SetScene1Plus5 Window={0} Cam{1} {2} {3} {4} {5} {6}\r\n", window, camera1, camera2, camera3, camera4, camera5, camera6);
            CrestronConsole.Print("Body = {0}", body);

            SendPut(baseURL + "/ISAPI/DisplayDev/VideoWall/1/window/" + window, body);
            SendPut(baseURL + "/ISAPI/DisplayDev/VideoWall/1/window/" + window, body);      // send twice as sometimes not all cameras load on the screen, don't know why!
        }

        public void SetCamera(int wall, int scene, int window, int camera)
        {
            string body = @"<SubWindow xmlns=""http://www.isapi.org/ver20/XMLSchema"" version=""2.0""><id>" + window + "</id><SubWindowParam><signalMode>stream id</signalMode><streamingChannelID>" + MyCameraList[camera].id + "</streamingChannelID></SubWindowParam></SubWindow>";            
            CrestronConsole.Print("Set Camera Wall={0} Scene={1} Window={2} Cam={3}\r\n", wall, scene, window, MyCameraList[camera].id);

            baseURL = String.Format(@"http://{0}:{1}", address, port);
            SendPut(baseURL + "/ISAPI/DisplayDev/VideoWall/1/window/" + wall + "/sub/" + window, body);
        }

        public void reboot()
        {
            string body = "";
            CrestronConsole.Print("System Reboot");

            baseURL = String.Format(@"http://{0}:{1}", address, port);
            SendPut(baseURL + "/ISAPI/System/reboot", body);        
        }

        private string SendPut(String urlCmdPath, String body)
        {
            CrestronConsole.Print("Put Request Called\r\n");

            //Send Put request
            try
            {
                Uri myUri = new Uri(urlCmdPath);
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(myUri);

                NetworkCredential myNetworkCredential = new NetworkCredential(username, password);
                CredentialCache myCredentialCache = new CredentialCache();
                myCredentialCache.Add(myUri, "Basic", myNetworkCredential);
                myHttpWebRequest.PreAuthenticate = true;
                myHttpWebRequest.Credentials = myCredentialCache;

                myHttpWebRequest.Method = "PUT";
                byte[] bytes = Encoding.ASCII.GetBytes(body);
                myHttpWebRequest.ContentLength = bytes.Length;

                Stream requestStream = myHttpWebRequest.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Flush();
                requestStream.Close();

                WebResponse myWebResponse = myHttpWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                string pageContent = myStreamReader.ReadToEnd();
                responseStream.Close();
                myWebResponse.Close();

                //Print the data that was returned from the Post request
                CrestronConsole.Print("Data that was returned from Post request: " + pageContent + "\r\n");
                return pageContent;
            }
            catch (Exception e)
            {
                CrestronConsole.Print("WebClient Post Request Exception Thrown: {0}\r\n", e);
                return null;
            }
        }

        private void SendPost(String urlCmdPath, String body)
        {      
            CrestronConsole.Print("POST Request Called\r\n");

            try
            {
                HttpClientRequest MyHttpClientRequest = new HttpClientRequest();
                HttpClientResponse MyHttpClientResponse;
                CrestronConsole.Print("POST Request Called 2\r\n");                
                MyHttpClientRequest.Url.Parse(urlCmdPath);
                MyHttpClientRequest.ContentString = body;
                MyHttpClientRequest.RequestType = RequestType.Post;
                MyHttpClientRequest.Header.AddHeader(new HttpHeader("Content-Type", "text/xml"));  
                MyHttpClient.KeepAlive = true;
                MyHttpClient.TimeoutEnabled = true;
                MyHttpClient.Timeout = 10;
                MyHttpClient.UserName = username;
                MyHttpClient.Password = password;
                CrestronConsole.Print("POST Request Called 3\r\n");                               
                            
                using(MyHttpClientResponse = MyHttpClient.Dispatch(MyHttpClientRequest))
                {
                    CrestronConsole.PrintLine("HTTP Client SendPost Response={0}\n\r", MyHttpClientResponse.ContentString);
                    //return MyHttpClientResponse;
                }
            }
            catch (Exception ex)
            {
                CrestronConsole.Print("HttpClient Post Request Exception Thrown: {0}\r\n", ex);                
            }
        }

        private void SendGet(String urlCmdPath)
        {
            String returnedFromGet = "";
            int counter = 0;
            CrestronConsole.Print("SendGet Called");

            returnedFromGet = MySimplSharpWebClient.SendGet(address, port, urlCmdPath, username, password);
            CrestronConsole.Print("SendGet String Returned: " + returnedFromGet + "\r\n");

            try
            {
                CrestronConsole.PrintLine("Extractor Starting...\n\r");    //Generate error

                XmlReader reader = XmlReader.Create(new StringReader(returnedFromGet));
                CrestronConsole.PrintLine("XML Reader Create\n\r");

                reader.Read();      // read xml header
                reader.Read();      // read blank (not sure why it's blank.....
                reader.Read();      // read opening tag

                switch (reader.Name)
                {
                    case "WallSceneList":
                        {
                            CrestronConsole.PrintLine("XML Reader 'WallSceneList' found\n\r");
                            while (reader.Read())
                            {
                                if (reader.IsStartElement())
                                {
                                    switch (reader.Name)
                                    {                                        
                                        case "id":
                                            CrestronConsole.PrintLine("XML Reader 'id' found\n\r");
                                            if (reader.Read())
                                            {
                                                MySceneList[counter].id = Convert.ToUInt16(reader.Value.Trim());
                                                CrestronConsole.PrintLine("Scene{0} ID: {1}\n\r", counter, MySceneList[counter].id);    //Generate error
                                            }                                            
                                            break;
                                        case "name":
                                            CrestronConsole.PrintLine("XML Reader 'name' found\n\r");
                                            if (reader.Read())
                                            {
                                                MySceneList[counter].name = reader.Value.Trim();
                                                CrestronConsole.PrintLine("Scene{0} Name: {1}\n\r", counter, MySceneList[counter].name);    //Generate error
                                            }
                                            counter++;
                                            break;
                                    }
                                }
                            }
                            CrestronConsole.PrintLine("SceneListLength = {0}\n\r", MySceneList.Length);    //Generate error 
                            for (int i = 0; i < MySceneList.Length; i++)
                            {
                                if (MySceneList[i].name == null)
                                {
                                    break;
                                }
                                else
                                {
                                    CrestronConsole.PrintLine("SceneList{0} Name={1} ID={2}\n\r", i, MySceneList[i].name, MySceneList[i].id);    //Generate error 
                                    OutputScene(MySceneList[i].name, MySceneList[i].id, Convert.ToUInt16(i));
                                }
                            }
                        }
                        break;
                    case "StreamInputChannelList":
                        {
                            CrestronConsole.PrintLine("XML Reader 'StreamInputChannelList' found\n\r");
                            while (reader.Read())
                            {
                                if (reader.IsStartElement())
                                {
                                    switch (reader.Name)
                                    {
                                        case "name":
                                            CrestronConsole.PrintLine("XML Reader 'name' found\n\r");
                                            if (reader.Read())
                                            {
                                                MyCameraList[counter].name = reader.Value.Trim();
                                                CrestronConsole.PrintLine("Cam{0} Name: {1}\n\r", counter, MyCameraList[counter].name);    //Generate error
                                            }
                                            break;
                                        case "id":
                                            CrestronConsole.PrintLine("XML Reader 'id' found\n\r");
                                            if (reader.Read())
                                            {
                                                MyCameraList[counter].id = Convert.ToUInt16(reader.Value.Trim());
                                                CrestronConsole.PrintLine("Cam{0} ID: {1}\n\r", counter, MyCameraList[counter].id);    //Generate error
                                            }
                                            break;
                                        case "group":
                                            CrestronConsole.PrintLine("XML Reader 'group' found\n\r");
                                            if (reader.Read())
                                            {
                                                MyCameraList[counter].group = reader.Value.Trim();
                                                CrestronConsole.PrintLine("Cam{0} Group: {1}\n\r", counter, MyCameraList[counter].group);    //Generate error
                                                counter++;
                                            }
                                            break;
                                    }
                                }
                            }
                            CrestronConsole.PrintLine("CameraListLength = {0}\n\r", MyCameraList.Length);    //Generate error 
                            for (int i = 0; i < MyCameraList.Length; i++)
                            {
                                if (MyCameraList[i].name == null)
                                {
                                    break;
                                }
                                else
                                {
                                    CrestronConsole.PrintLine("CameraList{0} Name={1} ID={2} Group={3}\n\r", i, MyCameraList[i].name, MyCameraList[i].id, MyCameraList[i].group);    //Generate error 
                                    OutputCamera(MyCameraList[i].name, MyCameraList[i].id, MyCameraList[i].group, Convert.ToUInt16(i));
                                }
                            }
                        }
                        break;
                }                
                CrestronConsole.PrintLine("Extractor Finished...\n\r");    //Generate error                 
            }
            catch (Exception e)
            {
                CrestronConsole.Print("XML Exception Thrown: {0}\r\n", e);
            }
        }
    }
}
