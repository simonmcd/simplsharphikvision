﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace HikvisionLibrary
{
    public class Scene
    {
        public Scene()
        {
        }

        public string name
        {
            get;
            set;
        }

        public ushort id
        {
            get;
            set;
        }
    }
}